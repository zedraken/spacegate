#include "AudioSource.h"
#include "macros.h"

// The AudioSource class inherits from the Urho3D SoundSource class.
AudioSource::AudioSource(Context *context) : SoundSource(context)
{
    SetSoundType(SOUND_VOICE);
    SetGain(1.0);
    SubscribeToEvent(E_SOUNDFINISHED, URHO3D_HANDLER(AudioSource, PlaybackEndEvent));
    SubscribeToEvent(E_POSTUPDATE, URHO3D_HANDLER(AudioSource, PostUpdateEvent));
    mElapsedTime = 0.0;
    mStartTime = 0.0;
    mDelayPlayback = false;
}

AudioSource::~AudioSource()
{
    //dtor
}

void AudioSource::Playback(SharedPtr<Sound> sample)
{
    // Push new sample into samples list.
    mSampleList.Push(sample);
    // If no samples are currently playing, then pop oldest one from the list
    // and plays it! Otherwise, do not do anything.
    if(!IsPlaying())
    {
        Play(mSampleList.Front());
    }
}


void AudioSource::PlaybackEndEvent(StringHash eventType, VariantMap& eventData)
{
    // Last sample playing is finished. Get next one, if any.
    mSampleList.PopFront();
    // If there is a sample to be played from the list, play it!
    if(!mSampleList.Empty())
    {
        Play(mSampleList.Front());
    }
}

void AudioSource::PostUpdateEvent(StringHash eventType, VariantMap& eventData)
{
    using namespace PostUpdate;

    float time_step = eventData[P_TIMESTEP].GetFloat();
    mElapsedTime += time_step;

    if(mElapsedTime - mStartTime >= 5.0)
    {
        mStartTime = mElapsedTime;
    }
}

float AudioSource::GetElapsedTime()
{
    return mElapsedTime;
}