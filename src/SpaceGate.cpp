
#include "MainApp.h"
#include "SpaceGate.h"

SpaceGate::SpaceGate(Scene *parent, ResourceCache *resource, String modelfile)
{
    mNum = 0;
    mParent = parent;
    mResource = resource;

    // Create the group.
    mGroupNode = parent->CreateChild("SpaceGatesGroup");
    mGroupModel = mGroupNode->CreateComponent<StaticModelGroup>();
    mGroupModel->SetModel(resource->GetResource<Model>(modelfile));
    mGroupModel->ApplyMaterialList();
    mGroupModel->SetCastShadows(true);

    // SETTING UP LIGHT ANIMATION FOR GATES
    Material *matBeacon = resource->GetResource<Material>("Materials/Beacon1.xml");
    if(!matBeacon)
    {
        LOGE("Material file not found");
        return;
    }
    SharedPtr<ValueAnimation> colorAnimation(new ValueAnimation(parent->GetContext()));
    Color beaconYellowColor(1.0, 0.9, 0.1);
    Color beaconWhiteColor(1.0, 1.0, 1.0);
    Color beaconOffColor(0.0, 0.0, 0.0);
    // Light animation scheme is : L(50ms)H(50ms)L(50ms)H(50ms)L(1.75s)
    colorAnimation->SetKeyFrame(0.0f, beaconOffColor);
    colorAnimation->SetKeyFrame(0.05f, beaconYellowColor);
    colorAnimation->SetKeyFrame(0.10f, beaconOffColor);
    colorAnimation->SetKeyFrame(0.15f, beaconOffColor);
    colorAnimation->SetKeyFrame(0.20f, beaconWhiteColor);
    colorAnimation->SetKeyFrame(0.25f, beaconOffColor);
    colorAnimation->SetKeyFrame(2.0f, beaconOffColor);
    matBeacon->SetScene(parent);
    matBeacon->SetShaderParameterAnimation("MatDiffColor", colorAnimation);
    matBeacon->SetShaderParameterAnimation("MatSpecColor", colorAnimation);
    matBeacon->SetShaderParameterAnimation("MatEmissiveColor", colorAnimation);
}

SpaceGate::~SpaceGate()
{
    //dtor
}

void SpaceGate::AddGate()
{
    Node *node = gateCreateAndAdd();
}

void SpaceGate::AddGate(Vector3 position, Quaternion rotation)
{
    Node *node = gateCreateAndAdd();
    node->SetPosition(position);
    node->SetRotation(rotation);
}

void SpaceGate::AddGate(Vector3 position)
{
    Node *node = gateCreateAndAdd();
    node->SetPosition(position);
}

unsigned int SpaceGate::GetNumInstances()
{
    return(mGroupModel->GetNumInstanceNodes());
}

Node *SpaceGate::gateCreateAndAdd()
{
    int num = mNum++;

    String gate_name = "Gate" + String(num);
    Node *gate_node = mParent->CreateChild(gate_name);
    RigidBody *gate_body = gate_node->CreateComponent<RigidBody>();
    CollisionShape *gate_shape = gate_node->CreateComponent<CollisionShape>();
    gate_shape->SetTriangleMesh(mGroupModel->GetModel());
    BoundingBox bbox = mGroupModel->GetBoundingBox();
    gateNodes_.Push(SharedPtr<Node>(gate_node));
    mGroupModel->AddInstanceNode(gate_node);

    // Create a node that will be used as a trigger to detect collisions with
    // a spaceship without physical actions (it can be traversed as if it is
    // empty). It will only trigger collision events.
    String trigate_name = "TriggerGate" + String(num);
    Node *trigger_node = gate_node->CreateChild(trigate_name);
    RigidBody *trigate_body = trigger_node->CreateComponent<RigidBody>();
    CollisionShape *trigate_shape = trigger_node->CreateComponent<CollisionShape>();
    trigate_shape->SetBox(Vector3(30.0, 30.0, 1.0));
    trigate_body->SetTrigger(true);
    trigate_body->SetKinematic(true);

    return gate_node;
}

Node *SpaceGate::GetNode(unsigned index)
{
    return gateNodes_.At(index);
}
