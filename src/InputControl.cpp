#include "MainApp.h"

void SpaceGateApp::HandleKeyDown(StringHash eventType, VariantMap& eventData)
{
    using namespace KeyDown;
    int key = eventData[P_KEY].GetInt();
    int qual = eventData[P_QUALIFIERS].GetInt();

    switch(key)
    {
        case KEY_ESCAPE:
            engine_->Exit();
            break;

        case KEY_SPACE:
            break;

        case KEY_F1:
            mGameHelpText->SetVisible(!mGameHelpText->IsVisible());
            if(mGameHelpText->IsVisible())
                LOGI("Help text is visible");
            else
                LOGI("Help text is not visible");
            break;

        case KEY_F2:
            GetSubsystem<Graphics>()->ToggleFullscreen();
            break;

        case KEY_F10:
            debug_render = !debug_render;
            mScene->GetSubsystem<Renderer>()->DrawDebugGeometry(debug_render);
            break;

        case KEY_F11:
            ToggleMouseVisible();
            break;

        case KEY_F12:
            if(mDebugHud->GetMode() != DEBUGHUD_SHOW_ALL)
                mDebugHud->SetMode(DEBUGHUD_SHOW_ALL);
            else
                mDebugHud->SetMode(DEBUGHUD_SHOW_NONE);
            break;

        case KEY_PAGEUP:
            mParentIndex++;
            if(mParentIndex > 1)
                mParentIndex = 0;
            switch(mParentIndex)
            {
            case 0:
                mCameraNode->SetParent(mModelNode);
                break;
            case 1:
                mCameraNode->SetParent(mSpaceCruiser->GetNode());
                break;
            }
            break;

        case KEY_KP_MINUS:
            break;

        case KEY_TAB:
            if(qual == 0)
            {
                SelectNextTarget();
                mRadioSource->Playback(mSoundTargetLocked);
            }
            else if(qual == QUAL_CTRL)
            {
                SelectPreviousTarget();
                mRadioSource->Playback(mSoundTargetLocked);
            }
            // Play a sound
            /*
            mSoundSource->Play(mSoundBeep1);
            mSoundSource->Play(mSoundTargetLocked);
            */
            break;

        case KEY_B:
            if(qual == 0)
                mTrackedObject = NULL;
            break;

        case KEY_A:
            if(qual == 0)
            {
                mHeadDownDisplayToggle = !mHeadDownDisplayToggle;
                if(mHeadDownDisplayToggle)
                    mHeadDownDisplayViewPort->SetCamera(mRearCamera);
                else
                    mHeadDownDisplayViewPort->SetCamera(mFrontCamera);
            }
            break;

        case KEY_R:
            if(qual == QUAL_CTRL)
            {
                SelectPreviousRadarRange();
            }
            else if(qual == 0)
            {
                SelectNextRadarRange();
            }
            break;

        default:
            break;
    };
}


void SpaceGateApp::JoystickButtonDown(StringHash eventType, VariantMap& eventData)
{
    using namespace JoystickButtonDown;

    int joystickId = eventData[P_JOYSTICKID].GetInt();
    int buttons = eventData[P_BUTTON].GetInt();

    LOGI("Joystick buttans state: " + String(buttons));
    // LOGI("Joystick buttons down : " + String(buttons + 1));
    if(buttons == 2)
    {
        // Reset camera orientation and position.
        mPitch = 0.0;
        mYaw = 0.0;
        mCameraNode->SetPosition(CAMPOS);
        // Reset linear and angular velocities
        mShipBody->SetAngularVelocity(Vector3::ZERO);
        mShipBody->SetLinearVelocity(Vector3::ZERO);

    }

    if(buttons == 3)
    {
        LOGI("Button 3 down");
    }
}

void SpaceGateApp::JoystickButtonUp(StringHash eventType, VariantMap& eventData)
{
    using namespace JoystickButtonUp;

    int jid = eventData[P_JOYSTICKID].GetInt();
    int buttons = eventData[P_BUTTON].GetInt();

    // LOGI("Joystick buttons up : " + String(buttons + 1));
    if(buttons == 3)
    {
        LOGI("Button 3 up");
    }
}

void SpaceGateApp::MouseWheelEvent(StringHash eventType, VariantMap& eventData)
{
    using namespace MouseWheel;

    int wheel = eventData[P_WHEEL].GetInt();
    int buttons = eventData[P_BUTTONS].GetInt();
    int qualifiers = eventData[P_QUALIFIERS].GetInt();

    if(qualifiers == QUAL_ALT)
    {
        if(mCamSpeed <= 10.0)
            mCamSpeed += (float)wheel;
        else if(mCamSpeed <= 100.0)
            mCamSpeed += (float)wheel * 10.0;
        else
            mCamSpeed += (float)wheel * 100.0;

        if(mCamSpeed < 0.0) mCamSpeed = 0.0;

        LOGI("New camera speed: " + String(mCamSpeed));
    }
    else if(qualifiers == QUAL_CTRL)
    {
        mDisplayZoom += (float)wheel;
        if(mDisplayZoom <= 0.5)
            mDisplayZoom = 0.5;
        if(mDisplayZoom >= 40.0)
            mDisplayZoom = 40.0;
        mFrontCamera->SetZoom(mDisplayZoom);
        LOGI("mDisplayZoom = " + String(mDisplayZoom));
    }
}

void SpaceGateApp::MouseButtonDown(StringHash eventType, VariantMap& eventData)
{
    using namespace MouseButtonDown;

    int button = eventData[P_BUTTON].GetInt();
    int qualifier = eventData[P_QUALIFIERS].GetInt();

    LOGI("mouse button: " + String(button));
    if(button == 4)
        ToggleMouseVisible();
}

void SpaceGateApp::JoystickAxisMove(StringHash eventType, VariantMap& eventData)
{
    using namespace JoystickAxisMove;

    int axis = eventData[P_AXIS].GetInt();
    float position = eventData[P_POSITION].GetFloat();

    // LOGI("Joystick axis: " + String(axis));

    if(axis == 3)
    {
        mShipSpeedRequest = (1 - position) * 250.0f;
    }

}

