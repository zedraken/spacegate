
#include <Urho3D/UI/MessageBox.h>
#include "MainApp.h"
#include "SpaceObject.h"
#include "SpaceGate.h"
#include "AudioSource.h"
#ifdef WIN32
#ifdef MessageBox
#undef MessageBox
#endif
#endif

using namespace Urho3D;

SpaceGateApp::SpaceGateApp(Context* context) : Application(context)
{
    debug_render = false;
    mYaw = 0.0f;
    mPitch = 0.0f;
    mCamSpeed = 5.0;
    mShipSpeed = 0.0f;
    mShipSpeedIncr = 15.0f;
    mShipSpeedRequest = 0.0;
    mShipYaw = 0;
    mShipPitch = 0;
    mShipRoll = 0;
    mShipYawIncr = 65.0;
    mShipPitchIncr = 70.0;
    mShipRollIncr = 105.0;
    mShipAngularThreshold = 0.5;
    mMouseVisible = false;
    mTimer = new Timer();
    mDisplayTimer = new Timer();
    mTrackedObject = NULL;
    mTrackedObjectIndex = 0;
    mNextGateToCross = 0;
    mAnimationAngleIndicator = 0.0f;
    mDisplayState = false;
    mMissionStarted = false;
    mMissionStartTime = 0;
    mMissionCurrentTime = 0;
    mRadarRangeIndex = 0; /* Default to 10 km */
    mRadarRangesList.Push(10);
    mRadarRangesList.Push(100);
    mRadarRangesList.Push(500);
    mDisplayZoom = 1.0;
    mHeadDownDisplayToggle = false;
    mParentIndex = 0;
    mShipBumped = false;
    mVirtualDisplay = false;
    mNoJoystick = true;

    // The AudioSource class is registered to be used as a regular component.
    context->RegisterFactory<AudioSource>();
}


void SpaceGateApp::Setup()
{
    LOGI("Entering engine initialization");



#ifdef __DEBUG__
    mWindowSize.x_ = 1920;
    mWindowSize.y_ = 1080;
    engineParameters_[Urho3D::EP_FULL_SCREEN] = false;
    engineParameters_[Urho3D::EP_WINDOW_RESIZABLE] = true;
    engineParameters_[Urho3D::EP_LOG_LEVEL] = LOG_DEBUG;
#else
    mWindowSize.x_ = 1920;
    mWindowSize.y_ = 1080;
    engineParameters_[Urho3D::EP_FULL_SCREEN] = true;
    engineParameters_[Urho3D::EP_WINDOW_RESIZABLE] = false;
    engineParameters_[Urho3D::EP_LOG_LEVEL] = LOG_INFO;
#endif // __DEBUG__

    engineParameters_[Urho3D::EP_WINDOW_WIDTH] = mWindowSize.x_;
    engineParameters_[Urho3D::EP_WINDOW_HEIGHT] = mWindowSize.y_;
	engineParameters_[Urho3D::EP_HEADLESS] = false;
	engineParameters_[Urho3D::EP_SOUND] = true;
    // engineParameters_["ResourcePrefixPaths"] = "/usr/local/share/";

}


void SpaceGateApp::Start()
{
    bool force_joystick = true;
    LOGI("Starting SpaceGate");

	   // Process custom command line arguments. Options are:
     // vr           : use of the VR headset
     // fullscreen   : toggle fullscreen
     // no-joystick  : disable joystick check
    ProcessCLIArguments();

    // Activate the log system
    Log* logsubsys = GetSubsystem<Log>();
    logsubsys->SetLevel(LOG_DEBUG);
    LOGI("Registering event handlers");

     // Create a new scene
	mScene = new Scene(context_);

    // Get a reference on the resource system (to read data from disk)
	mResourcesCache = GetSubsystem<ResourceCache>();

    // Get a pointer to the input subsystem.
    mInput = GetSubsystem<Input>();

    // Check for installed joystick(s)
    int jsnum = mInput->GetNumJoysticks();
    LOGI("Found " + String(jsnum) + " joysticks");
    if(jsnum != 0)
    {
        mJoystick = mInput->GetJoystickByIndex(0);
        LOGI("Found useable joystick !");
        LOGI("Num axis    : " + String(mJoystick->GetNumAxes()));
        LOGI("Num hats    : " + String(mJoystick->GetNumHats()));
        LOGI("Num buttons : " + String(mJoystick->GetNumButtons()));
    }
    else if(mNoJoystick == true)
    {
        // Load the custom message box style file
        XMLFile *layout = mResourcesCache->GetResource<XMLFile>("UI/SpaceMsgBox.xml");
        // Load the general style file
        XMLFile *style = mResourcesCache->GetResource<XMLFile>("UI/SpaceStyle.xml");
        // Display an error message box
        Urho3D::MessageBox *errMsgBox = new Urho3D::MessageBox(context_, String("No joystick found!\nClick OK to exit."), String("Error message"), layout, style);
        // Connect the handler to the 'OK' button
        SubscribeToEvent(errMsgBox, E_MESSAGEACK, URHO3D_HANDLER(SpaceGateApp, QuitMessageAck));
        // Set mouse visible so the user can click on the 'OK' button
        mInput->SetMouseVisible(true);
        // Abort start because no joystick is connected, and wait for the user to close the message box and quit the application.
        return;
    }

    // Display the splash screen
    SplashScreen();

    // Get a pointer to the audio subsystem.
    Audio *audio = GetSubsystem<Audio>();

	// Create an octree for space partitioning
	mScene->CreateComponent<Octree>();

	// Create the physics world we will need for collision detection
	mScene->CreateComponent<PhysicsWorld>();
    mScene->GetComponent<PhysicsWorld>()->DrawDebugGeometry(true);

	// Creation of a debug renderer
	mDebugRenderer = mScene->CreateComponent<DebugRenderer>();

	// Add local resource folder with highest search priority
	mResourcesCache->AddResourceDir("Data/", 0);

	// Add extra resources directories.
    mResourcesCache->AddResourceDir("Data/Blackswann/", 1);
    mResourcesCache->AddResourceDir("Data/StarCruiser/", 1);
    mResourcesCache->AddResourceDir("Data/Aetheron/", 1);
    mResourcesCache->AddResourceDir("Data/Asteroid/", 1);

	// Get a pointer to the UI root
	mUIRoot = GetSubsystem<UI>()->GetRoot();

    // mResourcesCache->GetResource<XMLFile>("UI/DefaultStyle.xml");
    SubscribeToEvent(E_KEYDOWN, URHO3D_HANDLER(SpaceGateApp, HandleKeyDown));
    SubscribeToEvent(E_UPDATE, URHO3D_HANDLER(SpaceGateApp, Update));
    SubscribeToEvent(E_RENDERUPDATE, URHO3D_HANDLER(SpaceGateApp, RenderUpdate));
    SubscribeToEvent(E_POSTRENDERUPDATE, URHO3D_HANDLER(SpaceGateApp, PostRenderUpdate));
    SubscribeToEvent(E_JOYSTICKBUTTONDOWN, URHO3D_HANDLER(SpaceGateApp, JoystickButtonDown));
    SubscribeToEvent(E_JOYSTICKBUTTONUP, URHO3D_HANDLER(SpaceGateApp, JoystickButtonUp));
    SubscribeToEvent(E_JOYSTICKAXISMOVE, URHO3D_HANDLER(SpaceGateApp, JoystickAxisMove));
    SubscribeToEvent(E_NODECOLLISIONSTART, URHO3D_HANDLER(SpaceGateApp, NodeCollisionStartEvent));
    SubscribeToEvent(E_MOUSEWHEEL, URHO3D_HANDLER(SpaceGateApp, MouseWheelEvent));
    SubscribeToEvent(E_MOUSEBUTTONDOWN, URHO3D_HANDLER(SpaceGateApp, MouseButtonDown));

#ifdef __DEBUG__
    mMouseVisible = true;
#endif

	mInput->SetMouseVisible(mMouseVisible);
	mInput->SetMouseGrabbed(false);


	// Get Graphics subsystem pointer.
    Graphics* graphics = GetSubsystem<Graphics>();
    graphics->SetWindowTitle("Space Gates !");
    graphics->SetWindowPosition(IntVector2(600,200));
	if(mVirtualDisplay == true) {
		graphics->ToggleFullscreen();
	}

    // Disable dynamic instancing if hardware does not support it.
#if 0
    if(!graphics->GetInstancingSupport())
    {
        Renderer *rdr = GetSubsystem<Renderer>();
        rdr->SetDynamicInstancing(false);
    }
#endif
    // Force dynamic instancing (due to some bugs)
    GetSubsystem<Renderer>()->SetDynamicInstancing(false);

    // Create the whole scene.
    CreateScene();

    // Create HUD symbols
    CreateHudSymbols();

    // Create UI elements.
    CreateInstructions();

    // Create (and load) the sounds
    CreateSounds();

    // Get position of the next gate to cross.
    mNextGateIndicator = mGates->GetNode(0)->GetPosition();
    // Display green arrow on next gate position.
    mArrowNode->SetPosition(mNextGateIndicator + Vector3(0,50,0));

    // Activate the display timer and reset it.
    mDisplayState = true;
    mDisplayTimer->Reset();

    // Create the audio listener (the camera position).
	mTheListener = mCameraNode->CreateComponent<SoundListener>();
	// Set the listener from the audio subsystem point of view
	audio->SetListener(mTheListener);
    // Start broadcasting SpaceCruiser engine sound.
    mSndSource3D1->Play(mSoundLowEngineRate);

    // Broadcast radio messages
    mRadioSource->Playback(mSoundGoodMorning);
    // mRadioSource->Playback(mSoundOnboardComputer);
}

void SpaceGateApp::Stop()
{

}


void SpaceGateApp::ToggleMouseVisible()
{
    mMouseVisible = !mMouseVisible;
    mInput->SetMouseVisible(mMouseVisible);
}

void SpaceGateApp::UpdateObjects(float time_step)
{
    SpaceObject *l_object;

    // Loop through all available objects
    for(int i = 0; i < mObjectsVector.Size(); i++)
    {
        // Build a pointer to the object
        l_object = static_cast<SpaceObject*> (mObjectsVector[i]);

        // Update object trajectory
        l_object->Update(time_step);
    }

    // Update HUD display symbols.
    UpdateHudDisplay();

}


void SpaceGateApp::NodeCollisionStartEvent(StringHash eventType, VariantMap& eventData)
{
    using namespace NodeCollisionStart;

    RigidBody *p1 = static_cast<RigidBody*>(eventData[P_BODY].GetPtr());
    RigidBody *p2 = static_cast<RigidBody*>(eventData[P_OTHERBODY].GetPtr());
    MemoryBuffer contact(eventData[P_CONTACTS].GetBuffer());

    contact.ReadVector3(); /* Read contact position */
    contact.ReadVector3(); /* Read contact normal */
    float distance = contact.ReadFloat(); /* Read contact distance */
    float impulse = contact.ReadFloat();

    LOGI("Contact distance = " + String(distance));
    LOGI("Contact impulse  = " + String(impulse));

    // Check if collider is the space ship.
    if(p1->GetNode()->GetName() == mModelNode->GetName())
    {
        // The second collider can be a gate or the asteroid or the cruise ship�
        if(p2->GetNode()->GetName() == "Asteroid")
        {
            // TODO: Play a sound�
            LOGI("Collision with Asteroid");
            // Play a sound
            mSoundSource->Play(mSoundCollision1);
            // Memorize the fact that the asteroid has collided with the ship
            this->mShipBumped = true;
        }
        else if(p2->GetNode()->GetName() == "StarCruiser")
        {
            LOGI("Collision with StarCruiser");
        }
        else
        {
            // Build gate name based on next gate to cross index.
            String gateName = String("TriggerGate") + String(mNextGateToCross);
            // LOGI("Collision event (" + gate + ") : " + String(p1->GetNode()->GetName()) + " with " + p2->GetNode()->GetName());

            // Compare built gate name with gate collider. If it is the same, then broadcast a successful sound
            // and increment next gate to cross index.
            if(gateName == p2->GetNode()->GetName())
            {
                // If this is the first gate, then start the mission.
                if(mNextGateToCross == 0)
                {
                    mMissionStarted = true;
                    mMissionStartTime = mTimer->GetMSec(false) / 1000;
                    LOGI("Mission started: " + String(mMissionStartTime));
                }

                // Check if the player goes through the gate in the right or in the opposito direction. For that, we
                // compute the dot product between the direction vectors of both the space ship and the gate. If the
                // product is negative, then the player enter the gate from the wrong side.
                Vector3 p1_dir = p1->GetNode()->GetDirection();
                Vector3 p2_dir = p2->GetNode()->GetWorldDirection();
                float dot = p1_dir.DotProduct(p2_dir);
                if(dot >= 0.0)
                {
                    mSoundSource->Play(mSoundSuccess);
                    mNextGateToCross++;
                    if(mNextGateToCross >= mGates->GetNumInstances())
                    {
                        // Reset next gate to cross (the player can try again).
                        mNextGateToCross = 0;
                        // Indicate that mission ended up.
                        mMissionStarted = false;
                        // Display an instructional message.
                        mInstructionText->SetText("Mission End !\nYour time is " + String((mTimer->GetMSec(false) / 1000) - mMissionStartTime) + " seconds");
                        mInstructionText->SetVisible(true);
                        mDisplayState = true;
                        // Reset instruction message display timer.
                        mDisplayTimer->Reset();

                    }
                    // Get position of the next gate to cross.
                    mNextGateIndicator = mGates->GetNode(mNextGateToCross)->GetPosition();
                    // Set position of the green arrow helper on top of the next gate to cross.
                    mArrowNode->SetPosition(mNextGateIndicator + Vector3(0,50,0));
                }
                else
                {
                    // If the dot product is negative, this means that the player crossed the gate
                    // in the reverse direction (the wrong one). We then play a specific sound.
                    mSoundSource->Play(mSoundWrongSide);
                }
            }
            else // otherwise, broadcast a radio message (the crossed gate is not the correct one).
            {
                mSoundSource->Play(mSoundWrongGate);
            }
        }

    }
}

void SpaceGateApp::QuitMessageAck(StringHash eventType, VariantMap& eventData)
{
    LOGI("Inside QuitMessageAck() handler !");
    engine_->Exit();
}

// Update the game logic
void SpaceGateApp::Update(StringHash eventType, VariantMap& eventData)
{
    using namespace Update;

    if(!mScene->IsUpdateEnabled())
        return;

    // Get elapsed time from last update
    float time_step = eventData[P_TIMESTEP].GetFloat();

    // Get a pointer to the input subsystem
    // Input *input = GetSubsystem<Input>();

    // Compute camera translation based on pressing arrow keys.
    if(mInput->GetKeyDown(KEY_UP))
        mCameraNode->Translate(Vector3::FORWARD * time_step * mCamSpeed);
    if(mInput->GetKeyDown(KEY_DOWN))
        mCameraNode->Translate(Vector3::BACK * time_step * mCamSpeed);
    if(mInput->GetKeyDown(KEY_RIGHT))
        mCameraNode->Translate(Vector3::RIGHT * time_step * mCamSpeed);
    if(mInput->GetKeyDown(KEY_LEFT))
        mCameraNode->Translate(Vector3::LEFT * time_step * mCamSpeed);

    // Update ship trajectory
    UpdateTrajectory(time_step);

    // Update external objects position and trajectory
    UpdateObjects(time_step);

    // Update green arrow indicator animation angle value.
    mAnimationAngleIndicator += 180.0f * time_step;
    mArrowNode->SetScale(Vector3(5.0, 5.0 + 1.0 * Cos(mAnimationAngleIndicator), 5.0));

    // Set front camera lookup at selected target (if any)
    if(mTrackedObject)
    {
        mFrontCameraNode->LookAt(mTrackedObject->GetPosition());
    }

    // Display debug informations if requested
    if(debug_render == true)
    {
        Vector3 p1org = Vector3::ZERO;
        Vector3 p1end = mModelNode->GetDirection() * 50.0f;
        mDebugRenderer->AddLine(p1org, p1end, Color(1,1,1), true);

        Vector3 p2org = mModelNode->GetPosition();
        mDebugRenderer->AddCross(p2org, 10.0f, Color(1,0,0), false);
        Vector3 p2end = p2org + mModelNode->GetDirection() * 20.0f;
        mDebugRenderer->AddCross(p2end, 10.0f, Color(0,1,0), false);
        mDebugRenderer->AddLine(p2org, p2end, Color(1,0.95,0.5), false);

        DrawWorldAxis();
    }


    // If mission is started, then update the mission counter display.
    if(mMissionStarted)
    {
        unsigned tmp = mTimer->GetMSec(false) / 1000;
        mChronoText->SetText("Mission time: " + String(tmp - mMissionStartTime));
    }
}

void SpaceGateApp::RenderUpdate(StringHash eventType, VariantMap& eventData)
{
    float time_step = eventData[RenderUpdate::P_TIMESTEP].GetFloat();

    // Compute camera orientation depending on mouse movements only if it is hidden (game mode).
    if(!mMouseVisible)
    {
        IntVector2 m = mInput->GetMouseMove();
        mYaw += 0.1f * m.x_;
        mPitch += 0.1f * m.y_;
    }

    // Compute camera orientation based on the camera pitch and yaw angles
    mCameraNode->SetRotation(Quaternion(mPitch, mYaw, 0.0f));

    // Handle temporary displayed text if any.
    if(mDisplayState)
    {
        if(mDisplayTimer->GetMSec(false) >= 7000)
        {
            mDisplayState = false;
            mInstructionText->SetVisible(false);
        }
    }
}

void SpaceGateApp::PostRenderUpdate(StringHash eventType, VariantMap& eventData)
{
    if(debug_render)
        mScene->GetComponent<PhysicsWorld>()->DrawDebugGeometry(true);
}

void SpaceGateApp::UpdateTrajectory(float time_step)
{
    float angle;
    float axis_position;

    // Handle joystick movements.
    if(mJoystick != NULL)
    {
        // Get joystick deviation on Z axis and compute roll angle.
        axis_position = mJoystick->GetAxisPosition(0);
        angle = ComputeAngularRotation(axis_position, mShipRollIncr, time_step);
        if(angle == 0.0) mShipRoll -= mShipRoll / mShipRollIncr;
        else mShipRoll += angle;

        // Get joystick deviation on X axis and compute pitch angle.
        axis_position = mJoystick->GetAxisPosition(1);
        angle = ComputeAngularPitch(axis_position, mShipPitchIncr, time_step);
        if(angle == 0.0) mShipPitch -= (mShipPitch / mShipPitchIncr);
        else mShipPitch += angle;

        // Get joystick deviation on Y axis and compute yaw angle.
        axis_position = mJoystick->GetAxisPosition(2);     // 10� per second for yaw rotation
        angle = ComputeAngularRotation(axis_position, mShipYawIncr, time_step);
        if(angle == 0.0) mShipYaw -= (mShipYaw / mShipYawIncr);
        else mShipYaw -= angle;
    }

    // Update ship speed based on requested speed.
    if(mShipSpeedRequest - mShipSpeed >= 0.2f)
    {
        mShipSpeed += mShipSpeedIncr * time_step;
    }
    else if(mShipSpeedRequest - mShipSpeed <= -0.2f)
    {
        mShipSpeed -= mShipSpeedIncr * time_step;
    }

    // If the ship has been collided by another object, then manage the ship random movements.
    if(mShipBumped == true)
    {
        // Smoothly reduce the angular velocity (in case of collision)
        Vector3 v = mShipBody->GetAngularVelocity();
        if (v.LengthSquared() >= 0.01)
        {
            // Smoothly decrease the angular velocity
            mShipBody->SetAngularVelocity(v * 0.997);
        }
        else
        {
            // Force angular velocity to zero
            mShipBody->SetAngularVelocity(Vector3::ZERO);
            // Collision effects are cancelled
            mShipBumped = false;
        }

        // Smoothly reduce the linear velocity (in case of collision)
        v = mShipBody->GetLinearVelocity();
        if (v.LengthSquared() >= 0.1)
            mShipBody->SetLinearVelocity(v * 0.997);
        else
            mShipBody->SetLinearVelocity(Vector3::ZERO);
    }


    // Apply rotation to main ship in its local space (not the parent space)
    Quaternion q = Quaternion(mShipPitch, mShipYaw, mShipRoll);
    mModelNode->Rotate(q);

    // Get ship current position
    Vector3 curpos = mModelNode->GetPosition();
    // Compute ship new position:
    //      new position = current position + direction vector * speed * time step
    Vector3 newpos = curpos + mModelNode->GetDirection() * mShipSpeed * time_step;
    // Update ship position
    mModelNode->SetPosition(newpos);

}



void SpaceGateApp::UpdateHudDisplay()
{
    float dot;

    // Update HUD indicators: timer, ship speed, ship roll rate
    mHudTimerText->SetText(String((int)(mTimer->GetMSec(false) / 1000)));
    mHudSpeedText->SetText(String((int)mShipSpeed));
    mHudRollText->SetText(String((int)(mShipRoll * 100)));
    mHudTargetID->SetText("TGT" + String(mTrackedObjectIndex));
    float r = (mShipSpeedRequest - mShipSpeed) * 0.05 / 500.0;
    mSpeedIndicator->SetPosition(HUD_CENTER_POS + Vector3(-0.025, r, 0.0));
    mHudRadarRange->SetText(String(mRadarRangesList[mRadarRangeIndex]));

    if(mTrackedObject)
    {
        /*
         *  UPDATE TRACKED TARGET RETICLE ON HUP
         */

        // Build a vector between the center position of the HUD and the pilot eyes position in world reference.
        Vector3 pn = mHudCenterPosition->GetWorldPosition() - mPilotEyePosition->GetWorldPosition();
        // Save length for further use.
        float length = pn.Length();
        // Normalize vector before using it for calculations. This will make calculation faster.
        pn.Normalize();
        // Build a vector between the pilot eyes position and the tracked object position in world reference.
        Vector3 vn = mTrackedObject->GetPosition() - mPilotEyePosition->GetWorldPosition();
        // Compute dot product between "vn" vector and the forward vector. This is used to know if the target
        // is behind the space ship, or in front.
        dot = vn.DotProduct(mModelNode->GetDirection());
        // If dot product is negative, then the target is behind the ship. Here, we display the reticle only
        // if the target is in front of the ship.
        if(dot > 0.0)
        {
            // Enable reticles
            mHudReticleTarget->SetEnabled(false);
            mHudTargetDirIndicator->SetEnabled(true);
            // Vector is normalized.
            vn.Normalize();
            // The angle between both vectors is computed.
            float a = pn.Angle(vn);
            // The reticle is displayed only if the angle is less than 35 degrees
            if(a <= 35.0)
                mHudReticleTarget->SetEnabled(true);

            // Compute the coefficient that will be applied to the VN vector to get the position of the reticle
            // on the plane defined by the HUD.
            float k = length / Cos(a);
            // Update reticle position on HUD: apply a factor to the "vn" vector from the pilot eye position,
            // to get the position of the reticle on the HUD plane that is in front of the pilot.
            Vector3 target_pos = mPilotEyePosition->GetWorldPosition() + vn * k;
            // Update the reticle position.
            mHudReticleTarget->SetWorldPosition(target_pos);

            /*
             *  UPDATE TARGET DIRECTION INDICATOR
             */
            // Get right vector (in model space).
            Vector3 right_vector = mModelNode->GetRight();
            // Build a vector between center of HUD and position of the target reticle on the HUD.
            Vector3 target_vector = target_pos - mHudCenterPosition->GetWorldPosition();
            // The vector is normalized.
            target_vector.Normalize();
            // Compute angle in degrees between those two vectors.
            a = target_vector.Angle(right_vector);
            // Since angle ranges from 0.0 to 180.0 and then from 180.0 to 0.0 (angle is never negative),
            // a fix shall be implemented. Depending on the dot product between the target vector and the model Up
            // vector, the angle is negated (if dot product is negative).
            dot = target_vector.DotProduct(mModelNode->GetUp());
            if(dot < 0.0f) a = 360.0f - a;
            // The angle (after it has been adjusted) is used to rotate the target direction indicator.
            mHudTargetDirIndicator->SetRotation(Quaternion(0.0f, 0.0f, a));
            // The target direction indicator is then displayed on the HUD. Since "target_vector" has been normalized,
            // its length is always 1 so the position of the reticle turns around HUD center position.
            mHudTargetDirIndicator->SetWorldPosition(mHudCenterPosition->GetWorldPosition() + target_vector / 14);
        }
        else
        {
            mHudReticleTarget->SetEnabled(false);
            mHudTargetDirIndicator->SetEnabled(false);
        }

        // Compute distance between ship and tracked target, and update the HUD.
        Vector3 l = mTrackedObject->GetPosition() - mModelNode->GetPosition();
        mHudDistText->SetText(String((int)l.Length()));
    }
}


URHO3D_DEFINE_APPLICATION_MAIN(SpaceGateApp);
