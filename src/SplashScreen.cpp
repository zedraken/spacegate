#include "MainApp.h"

void SpaceGateApp::SplashScreen()
{
    ResourceCache *cache = GetSubsystem<ResourceCache>();
    Graphics *graphics = GetSubsystem<Graphics>();
    UI *ui = GetSubsystem<UI>();

    BorderImage *splash = new BorderImage(context_);
    splash->SetName("Splash");
    Texture2D *texture = cache->GetResource<Texture2D>("Textures/splash_screen.png");
    splash->SetTexture(texture);
    splash->SetSize(graphics->GetWidth(), graphics->GetHeight());
    splash->SetAlignment(HA_CENTER, VA_CENTER);
    ui->GetRoot()->AddChild(splash);
    GetSubsystem<Engine>()->RunFrame();
    Timer *t = new Timer();
    while(t->GetMSec(false) < 3200);
    splash->Remove();
}

void SpaceGateApp::HandleSplashScreen(StringHash eventType, VariantMap& eventData)
{
    UIElement *splash = GetSubsystem<UI>()->GetRoot()->GetChild("Splash", true);
    if(splash)
    {
        splash->Remove();
        UnsubscribeFromEvent(E_ENDRENDERING);
    }
}
