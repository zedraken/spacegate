#include "MainApp.h"


void SpaceGateApp::ProcessCLIArguments()
{
  Urho3D::Vector<Urho3D::String> cli = GetArguments();
	if(cli.Size() > 0) {
		LOGI("Command line arguments");
		Urho3D::Vector<String>::Iterator it;
		for(it = cli.Begin(); it != cli.End(); it++) {
      LOGI(*it);
			if(*it == "no-joystick") {
        mNoJoystick = false;
			}
			if(*it == "vr") {
				mVirtualDisplay = true;
				LOGI("VR Headseat used");
			}
			if(*it == "fullscreen") {
        GetSubsystem<Graphics>()->ToggleFullscreen();
			}
		}
	}
}

void SpaceGateApp::PrintText(const char *str)
{
    PrintText(String(str));
}


void SpaceGateApp::PrintText(String str)
{
    UI *ui = GetSubsystem<UI>();
    ResourceCache* cache = GetSubsystem<ResourceCache>();

    Text *myText = ui->GetRoot()->CreateChild<Text>();

    myText->SetText(str);
    myText->SetFont(cache->GetResource<Font>("Fonts/BlueHighway.ttf"), 30);
    myText->SetColor(Color(1.0f, 1.0f, 1.0f));
    myText->SetHorizontalAlignment(HA_CENTER);
    myText->SetVerticalAlignment(VA_CENTER);
    LOGD(str);
}


void SpaceGateApp::DisplayInstructions()
{
    mGameHelpText->SetVisible(true);
}

void SpaceGateApp::SelectNextTarget()
{
    if(mTrackedObject)
    {
            mTrackedObjectIndex++;
            if(mTrackedObjectIndex >= mObjectsVector.Size())
                mTrackedObjectIndex = 0;
    }
    mTrackedObject = static_cast<SpaceObject*>(mObjectsVector[mTrackedObjectIndex]);
}

void SpaceGateApp::SelectPreviousTarget()
{
    if(mTrackedObject)
    {
        if(mTrackedObjectIndex == 0)
            mTrackedObjectIndex = mObjectsVector.Size() - 1;
        else
            mTrackedObjectIndex--;
    }
    mTrackedObject = static_cast<SpaceObject*>(mObjectsVector[mTrackedObjectIndex]);
}


void SpaceGateApp::SelectNextRadarRange()
{
    if(mRadarRangeIndex == mRadarRangesList.Size() - 1)
        mRadarRangeIndex = 0;
    else
        mRadarRangeIndex++;

    LOGI("New range: " + String(mRadarRangesList[mRadarRangeIndex]) + " km");
}

void SpaceGateApp::SelectPreviousRadarRange()
{
    if(mRadarRangeIndex == 0)
        mRadarRangeIndex = mRadarRangesList.Size() - 1;
    else
        mRadarRangeIndex--;

    LOGI("New range: " + String(mRadarRangesList[mRadarRangeIndex]) + " km");
}

float SpaceGateApp::ComputeAngularRotation(float deviation, float rotation_speed, float time_step)
{
    // LOGD("deviation : " + String(deviation) + ", rotation speed : " + String(rotation_speed));
    if(deviation >= mShipAngularThreshold)
    {
        return(-Pow((deviation - mShipAngularThreshold) * rotation_speed * time_step, 4.0f));
    }
    else if(deviation <= -mShipAngularThreshold)
    {
        return(Pow((deviation + mShipAngularThreshold) * rotation_speed * time_step, 4.0f));
    }
    return(0.0f);
}

float SpaceGateApp::ComputeAngularPitch(float deviation, float rotation_speed, float time_step)
{
    float threshold = 0.001;
    if(fabs(deviation) >= threshold)
    {
        float value = -Pow((deviation - threshold) * rotation_speed * time_step, 4.0f);
        return(Sign(deviation) * value);
    }
    return(0.0f);
}
