
#include "Segment.h"
#include "SpaceObject.h"


SpaceObject::SpaceObject(Scene *aParent, ResourceCache *aCache, String aModelName, String aModelFile)
{
    mModelName = aModelName;
    mModelFile = aModelFile;

    // Create the node
    mModelNode = aParent->CreateChild(aModelName);

    // The object is made of a static model
    mStaticModel = mModelNode->CreateComponent<StaticModel>();
    mStaticModel->SetModel(aCache->GetResource<Model>(aModelFile));
    mStaticModel->ApplyMaterialList();

    // Create an empty trajectory
    mTrajectory = new Urho3D::Vector<Segment*>;

    // Initialize absolute simulation time
    mAbsTime = 0.0f;

    mElevation = 0.0f;
    mBearing = 0.0f;

    mLocalYaw = 0.0f;
    mLocalPitch = 0.0f;
    mLocalRoll = 0.0f;
}

void SpaceObject::SetPosition(float aPosX, float aPosY, float aPosZ)
{
    mModelNode->SetPosition(Vector3(aPosX, aPosY, aPosZ));
}


void SpaceObject::SetSpeed(float aSpeed)
{
    mSpeed = aSpeed;
}

void SpaceObject::SetDirection(float aElevation, float aBearing)
{
    mElevation = -aElevation;
    mBearing = aBearing;
}

void SpaceObject::SetElevation(float aElevation)
{
    mElevation = -aElevation;
}

void SpaceObject::SetBearing(float aBearing)
{
    mBearing = aBearing;
}

void SpaceObject::SetRotation(float aYaw, float aPitch, float aRoll)
{
    mLocalYaw = aYaw;
    mLocalPitch = aPitch;
    mLocalRoll = aRoll;
}

void SpaceObject::SetYawAngle(float aAngle)
{
    mLocalYaw = aAngle;
}

void SpaceObject::SetPitchAngle(float aAngle)
{
    mLocalPitch = aAngle;
}

void SpaceObject::SetRollAngle(float aAngle)
{
    mLocalRoll = aAngle;
}

Vector3 SpaceObject::GetPosition()
{
    return mModelNode->GetPosition();
}

float SpaceObject::GetDistanceToOrigin()
{
    return(mModelNode->GetPosition().Length());
}

float SpaceObject::GetSpeed()
{
    return mSpeed;
}

float SpaceObject::GetElevation()
{
    return mElevation;
}

float SpaceObject::GetBearing()
{
    return mBearing;
}

void SpaceObject::Update(float aTimeStep)
{
    int i;

    // Compute absolute simulation time
    mAbsTime += aTimeStep;
    // LOGI("mAbsTime = " + String(mAbsTime));

    // Go through the trajectory to locate the right segment
    for(i = 0; i < mTrajectory->Size(); i++)
    {
        // If absolute time is in range of the current segment…
        if(mTrajectory->At(i)->IsInRange(mAbsTime))
        {
            // Compute orientation trajectory new elevation and bearing angles
            mElevation += mTrajectory->At(i)->GetElevationIncr() * aTimeStep;
            mBearing += mTrajectory->At(i)->GetBearingIncr() * aTimeStep;
            // Clamp angles into 0 to 360 range
            mElevation = fmodf(mElevation, 360.0);
            mBearing = fmodf(mBearing, 360.0);

            // Compute rotation new angles
            mLocalYaw += mTrajectory->At(i)->GetLocalYawIncr() * aTimeStep;
            mLocalYaw = fmodf(mLocalYaw, 360.0);
            mLocalPitch += mTrajectory->At(i)->GetLocalPitchIncr() * aTimeStep;
            mLocalPitch = fmodf(mLocalPitch, 360.0);
            mLocalRoll += mTrajectory->At(i)->GetLocalRollIncr() * aTimeStep;
            mLocalRoll = fmodf(mLocalRoll, 360.0);

            // LOGI("seg" + String(i) + " : (yaw ; pitch ; roll) = (" + String(mLocalYaw) + " ; " + String(mLocalPitch) + " ; " + String(mLocalRoll) + ")");

            // Compute new speed
            mSpeed += mTrajectory->At(i)->GetSpeedIncr() * aTimeStep;

            break;
        }
    }

    // Set object's orientation based on elevation and bearing angles.
    Quaternion orientation = Quaternion(mElevation, mBearing, 0);
    // Set object orientation in parent space. The object is oriented to face its forward vector
    // (defined by its elevation and bearing angles).
    mModelNode->SetRotation(orientation);

    // Rotate object around its local axis (nothing to deal with elevation and bearing angles).
    mModelNode->Rotate(Quaternion(mLocalYaw, mLocalPitch, mLocalRoll));

    // Compute the object forward vector depending on its orientation.
    Vector3 dir = orientation * Vector3::FORWARD;
    // The vector is normalized.
    dir.Normalize();
    // Compute new object position using current one and forward vector
    Vector3 newpos = mModelNode->GetPosition() + dir * mSpeed * aTimeStep;
    // Set object new computed position.
    mModelNode->SetPosition(newpos);
}


void SpaceObject::Enable()
{
    mModelNode->SetEnabled(true);
}

void SpaceObject::Disable()
{
    mModelNode->SetEnabled(false);
}

bool SpaceObject::IsEnabled()
{
    return mModelNode->IsEnabled();
}

void SpaceObject::AddSegment(Segment *aSegment)
{
    mTrajectory->Push(aSegment);

}

int SpaceObject::GetNumSegments()
{
    return mTrajectory->Size();
}

void SpaceObject::CreatePhysicalBody(float mass, ShapeType shape)
{
    // Create the physical body
    RigidBody *rb = mModelNode->CreateComponent<RigidBody>();
    // Set its mass (for forces to be applied)
    rb->SetMass(mass);
    // No gravity is used (to simplify interactions)
    rb->SetUseGravity(false);
    // Collision effects (false => report with effects, true => report only with no effects)
    rb->SetTrigger(false);

    // Create the collision shape
    CollisionShape *cs = mModelNode->CreateComponent<CollisionShape>();

    switch(shape)
    {
        case SHAPE_TRIANGLEMESH:
            // Set triangle mesh collision shape
            cs->SetTriangleMesh(mStaticModel->GetModel());
            break;
        default:
            // By default, the collision shape is the bounding box
            BoundingBox b = mStaticModel->GetBoundingBox();
            Vector3 v = b.max_ - b.min_;
            cs->SetBox(v);
            break;
    }

}

void SpaceObject::CreatePhysicalBody(float mass)
{
    // Create a physical body with a bounding box as the collision shape
    this->CreatePhysicalBody(mass, SHAPE_BOX);
}


