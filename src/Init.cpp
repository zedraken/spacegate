

#include "MainApp.h"
#include "SpaceGate.h"

const char *help_text = "SpaceGates instructions\n" \
"Your goal is to fly a small spaceship through a serie of space gates. You control the ship with the joystick and try to cross the gates one after one in the right order, and from the correct side.\n" \
"Your flight duration is recorded by a timer that starts at the time you cross the first gate, and stops when you cross the last one. " \
"The next gate to go through is indicated by a big green arrow that stretches back and forth to help localizing it. That arrow is updated to the next gate only when you have crossed the right one, and not another one! If you do not go through the indicated gate, the control center will send you a radio message.\n" \
"If you enter the gate from the wrong side, the control center will also sends you a radio message and you will still have to cross the same gate from the correct side.\n" \
"Once you have crossed the last gate, you have your time displayed for few seconds. Then, you can directly have another try by flying through the first gate again in which case the mission time record is reset for a new recording.\n" \
"You can move the mouse to look around to have a visual sight of the next gate in case you feel lost in the 3D space and you might need to slow down not to miss the next gate.\n" \
"All control commands are located on the joystick (movements, throttle and camera lookat reset).\n" \
"So, try to cross all the gates with a minimum flight duration and you will be considered as the best pilot of the galaxy !\n\n" \
"Useful keys are:\n" \
"Tab: cycle to next target\n" \
"Ctrl + Tab: cycle to previous target\n" \
"F1: display that help\n" \
"F2: toggle between window and fullscreen\n" \
"F10: toggle debug geometries on/off\n" \
"F11: display/hide mouse pointer (hidden by default)\n" \
"F12: display plenty of debug informations from Urho3D\n"
;

void SpaceGateApp::CreateScene()
{

    mDebugHud = engine_->CreateDebugHud();
    XMLFile *style = mResourcesCache->GetResource<XMLFile>("UI/HudStyle.xml");
    mDebugHud->SetDefaultStyle(style);

  	// Create a zone for lighting
  	Node* zoneNode = mScene->CreateChild("Zone");
  	mZone = zoneNode->CreateComponent<Zone>();
  	mZone->SetBoundingBox(BoundingBox(-50000.0f, 50000.0f));
  	mZone->SetAmbientColor(Color(0.15f, 0.15f, 0.15f));
  	mZone->SetAmbientGradient(true);
  	mZone->SetFogStart(800.0f);
  	mZone->SetFogEnd(10000.0f);

  	// Create lights
  	Node* l1Node = mScene->CreateChild("Light I");
  	l1Node->SetPosition(Vector3(0.0f, 500.0f, -500.0f));
  	l1Node->SetDirection(Vector3(0.0f, 0.0f, 0.0f));
  	Light* l1 = l1Node->CreateComponent<Light>();
  	l1->SetLightType(LIGHT_POINT);
  	l1->SetColor(Color(1.0f, 1.0f, 1.0f, 1.0f));
  	l1->SetCastShadows(true);
  	l1->SetRange(50000.0f);
  	l1->SetBrightness(2.45f);
  	l1->SetShadowBias(BiasParameters(0.00025f, 0.5f));
    l1->SetShadowCascade(CascadeParameters(1.0f, 5.0f, 10.0f, 10.0f, 0.8f));

/*
	Node* l2Node = mScene->CreateChild("Light II");
	Light* l2 = l2Node->CreateComponent<Light>();
	l2->SetLightType(LIGHT_POINT);
	l2->SetColor(Color(1.0f, 1.0f, 1.0f));
	l2->SetCastShadows(true);
	l2->SetRange(50000.0f);
	l2Node->SetPosition(Vector3(0.0f, -1000.0f, 0.0f));
*/
  	// Creation of the "BlackSwann" model
  	mModelNode = mScene->CreateChild("BlackSwann");
  	mModelNode->SetPosition(Vector3(0.0f, 0.0f, 0.0f));
  	modelA = mModelNode->CreateComponent<StaticModel>();
  	modelA->SetModel(mResourcesCache->GetResource<Model>("Models/Blackswann.mdl"));
  	modelA->ApplyMaterialList();
  	modelA->SetCastShadows(true);
  	modelA->SetOccluder(true);
  	Quaternion q = Quaternion(mShipYaw, mShipPitch, mShipRoll);
  	mModelNode->SetRotation(q);
  	mShipBody = mModelNode->CreateComponent<RigidBody>();
  	// mShipBody->SetAngularFactor(Vector3::ZERO);
  	mShipBody->SetRestitution(0.10f);
  	mShipBody->SetRotation(q);
  	mShipBody->SetMass(2000.0f);
  	mShipBody->SetFriction(10.0f);
  	mShipBody->SetUseGravity(false);
  	mShipBody->SetCollisionLayer(2);
  	mShipBody->SetTrigger(false);
  	// Create a collision shape for the ship
  	mCollisionShape = mModelNode->CreateComponent<CollisionShape>();
  	// Collision shape = bounding box
  	BoundingBox bbox = modelA->GetBoundingBox();
    Vector3 vbox = bbox.max_ - bbox.min_;
    mCollisionShape->SetBox(vbox);

    // Loading instrument panel (interior cockpit)
    StaticModel *instrument_panel = mModelNode->CreateComponent<StaticModel>();
    instrument_panel->SetModel(mResourcesCache->GetResource<Model>("Models/InstrumentPanel.mdl"));
    instrument_panel->ApplyMaterialList();
    instrument_panel->SetCastShadows(false);
    instrument_panel->SetOccluder(false);

    // Create particles system for exhaust
    Node *left_exhaust_node = mModelNode->CreateChild("Left Exhaust");
    left_exhaust_node->SetPosition(Vector3(-2.43654, 0.2, -3.83847));
    mLeftExhaust = left_exhaust_node->CreateComponent<ParticleEmitter>();
    mLeftExhaust->SetEffect(mResourcesCache->GetResource<ParticleEffect>("Particle/exhaust03.xml"));
    Node *right_exhaust_node = mModelNode->CreateChild("Right Exhaust");
    right_exhaust_node->SetPosition(Vector3(2.43654, 0.2, -3.83847));
    mRightExhaust = right_exhaust_node->CreateComponent<ParticleEmitter>();
    mRightExhaust->SetEffect(mResourcesCache->GetResource<ParticleEffect>("Particle/exhaust03.xml"));

    // CREATION OF THE SPACEBOX
    Node* skyNode = mScene->CreateChild("Skybox");
    Skybox* skybox = skyNode->CreateComponent<Skybox>();
    skybox->SetModel(mResourcesCache->GetResource<Model>("Models/Box.mdl"));
    skybox->SetMaterial(mResourcesCache->GetResource<Material>("Materials/nebulae.xml"));

      // CREATING FRONT FORWARD CAMERA
  	// mCameraNode = mScene->CreateChild("Camera");
  	mCameraNode = mModelNode->CreateChild("Camera");
  	mCamera = mCameraNode->CreateComponent<Camera>();
  	mCamera->SetFarClip(10000.0f);
  	// mCameraNode->SetPosition(Vector3(0.0f, 0.4788176, 2.66945));
  	mCameraNode->SetPosition(CAMPOS);
  	mCameraNode->LookAt(mModelNode->GetPosition());
  	mCamera->SetFillMode(FILL_SOLID);
  	mCamera->SetOrthographic(false);

  	// FRONT OPTICAL CAMERA
  	mFrontCameraNode = mModelNode->CreateChild("Front Camera");
  	mFrontCameraNode->SetPosition(CAMPOSFRONT);
  	mFrontCamera = mFrontCameraNode->CreateComponent<Camera>();
  	mFrontCamera->SetViewOverrideFlags(VO_DISABLE_OCCLUSION | VO_DISABLE_SHADOWS | VO_LOW_MATERIAL_QUALITY);
  	mFrontCamera->SetZoom(mDisplayZoom);
  	mFrontCamera->SetFarClip(10000.0);

  	// REAR BACKWARD CAMERA
  	mRearCameraNode = mModelNode->CreateChild("Rear camera");
  	// mRearCameraNode->SetPosition(Vector3(0, 0.51897, -3.1775));
  	mRearCameraNode->SetPosition(Vector3(0, 0.56875, -2.73841));
  	mRearCameraNode->Rotate(Quaternion(180.0f, Vector3::UP));
  	mRearCamera = mRearCameraNode->CreateComponent<Camera>();
  	mRearCamera->SetViewOverrideFlags(VO_DISABLE_OCCLUSION);
  	// mRearCamera->SetFov(90.0f);
  	mRearCamera->SetZoom(1.0f);
  	mRearCamera->SetFarClip(10000.0f);
  	// mRearCamera->SetFillMode(FILL_WIREFRAME);

	// Create Head Down Display screen on the instrument panel
    {
        Node *displayNode = mModelNode->CreateChild("Head Down Display");
        StaticModel *displayScreen = displayNode->CreateComponent<StaticModel>();
        displayScreen->SetModel(mResourcesCache->GetResource<Model>("Models/HeadDownDisplay.mdl"));

        SharedPtr<Texture2D> renderTexture(new Texture2D(context_));
        renderTexture->SetSize(640, 480, Graphics::GetRGBAFormat(), TEXTURE_RENDERTARGET);
        renderTexture->SetFilterMode(Urho3D::FILTER_BILINEAR);

        SharedPtr<Material> renderMaterial(new Material(context_));
        renderMaterial->SetTechnique(0, mResourcesCache->GetResource<Technique>("Techniques/GreenDisplay.xml"));
        renderMaterial->SetTexture(TU_DIFFUSE, renderTexture);
        renderMaterial->SetOcclusion(false);

        // Set object material
        displayScreen->SetMaterial(renderMaterial);

        // Get the rendering surface from the rendering texture
        RenderSurface *renderSurface = renderTexture->GetRenderSurface();

        // Create a specific viewport for the display
        mHeadDownDisplayViewPort = new Viewport(context_, mScene, mFrontCamera);

        // Assign viewport to the display
        renderSurface->SetViewport(0, mHeadDownDisplayViewPort);
    }



	// Pilot eye is a node which parent is the space ship. It is at the same position
	// than the camera. It used to compute position of items on HUD.
	mPilotEyePosition = mModelNode->CreateChild("PilotEye");
	mPilotEyePosition->SetPosition(CAMPOS);

    // CREATE OTHER OBJECTS
    CreateObjects();

	// Create the view port
	mViewPort = new Viewport(context_, mScene, mCamera);
    // Configure the renderer with the viewport
	GetSubsystem<Renderer>()->SetViewport(0, mViewPort);

	#if 0
	// Create camera and viewport for HUD
	Node *hudnode = mModelNode->CreateChild("HUDcamera");
	hudnode->SetPosition(CAMPOS + Vector3(0.0, 1.0, -5.0));
	Camera *hud_cam = hudnode->CreateComponent<Camera>();
	Viewport *hud_viewport = new Viewport(context_, mScene, hud_cam);
	IntRect r;
	r.left_ = mWindowSize.x_ / 2 - 200;
	r.right_ = mWindowSize.x_ / 2 + 200;
	r.top_ = mWindowSize.y_ / 2 - 200;
	r.bottom_ = mWindowSize.y_ / 2 + 200;
	hud_viewport->SetRect(r);
	// hud_viewport->SetPosition()
	GetSubsystem<Renderer>()->SetViewport(1, hud_viewport);
	#endif // 0

}


void SpaceGateApp::CreateObjects()
{
    SpaceObject *obm;
    Segment *seg;

/*
    _   ___ _____ ___ ___  ___ ___ ___
   /_\ / __|_   _| __| _ \/ _ \_ _|   \
  / _ \\__ \ | | | _||   / (_) | || |) |
 /_/ \_\___/ |_| |___|_|_\\___/___|___/

*/

    obm = new SpaceObject(mScene, mResourcesCache, "Asteroid", "Models/Asteroid.mdl");
    obm->CreatePhysicalBody(100000.0);
    obm->SetDirection(0, -90);
    obm->SetRotation(0, 180, 50);
    obm->SetPosition(500, 0, 10);
    obm->SetSpeed(100.0f);

    // Create a trajectory segment for the asteroid
    seg = new Segment(0);
    seg->SetPitchIncr(180);
    seg->SetRollIncr(45);
    obm->AddSegment(seg);
    mObjectsVector.Push(obm);


/*
  ___ _____ _   ___    ___ ___ _   _ ___ ___ ___ ___
 / __|_   _/_\ | _ \  / __| _ \ | | |_ _/ __| __| _ \
 \__ \ | |/ _ \|   / | (__|   / |_| || |\__ \ _||   /
 |___/ |_/_/ \_\_|_\  \___|_|_\\___/|___|___/___|_|_\

*/

    mSpaceCruiser = new SpaceObject(mScene, mResourcesCache, "StarCruiser", "Models/StarCruiser.mdl");
    mSpaceCruiser->CreatePhysicalBody(20000.0, SHAPE_TRIANGLEMESH);
    mSpaceCruiser->SetDirection(0, 90);
    mSpaceCruiser->SetPosition(-2000, -100, 800);
    mSpaceCruiser->SetSpeed(30);
    // Creation of the Starcruiser engine sound source
    mSndSource3D1 = mSpaceCruiser->GetNode()->CreateComponent<SoundSource3D>();
    mSndSource3D1->SetNearDistance(100.0);
    mSndSource3D1->SetFarDistance(1350.0);
    mSndSource3D1->SetGain(1.75);

    // Create the StarCruiser exhaust particle system.
    Node *sc_node = mSpaceCruiser->GetNode();
    Node *left_exhaust = sc_node->CreateChild("SC Left Exhaust");
    left_exhaust->SetPosition(Vector3(-13.53615, 4.40762, -101.0));
    ParticleEmitter *left_emitter = left_exhaust->CreateComponent<ParticleEmitter>();
    left_emitter->SetEffect(mResourcesCache->GetResource<ParticleEffect>("Particle/starcruiser_exhaust.xml"));
    Node *right_exhaust = sc_node->CreateChild("SC Right Exhaust");
    right_exhaust->SetPosition(Vector3(13.53615, 4.40762, -101.0));
    ParticleEmitter *right_emitter = right_exhaust->CreateComponent<ParticleEmitter>();
    right_emitter->SetEffect(mResourcesCache->GetResource<ParticleEffect>("Particle/starcruiser_exhaust.xml"));

    // Create StarCruiser trajectory segments.
    seg = new Segment(0, 10);
    mSpaceCruiser->AddSegment(seg);

    seg = new Segment(10, 20);
    seg->SetRollIncr(5.0f);
    seg->SetBearingIncr(-1.0f);
    mSpaceCruiser->AddSegment(seg);

    seg = new Segment(20, 40);
    seg->SetBearingIncr(-2.5f);
    mSpaceCruiser->AddSegment(seg);

    seg = new Segment(40, 50);
    seg->SetRollIncr(-5.0f);
    seg->SetBearingIncr(-1.0);
    mSpaceCruiser->AddSegment(seg);

    seg = new Segment(50, 125);
    mSpaceCruiser->AddSegment(seg);

    seg = new Segment(125, 135);
    seg->SetRollIncr(-5.0);
    seg->SetBearingIncr(1.0);
    mSpaceCruiser->AddSegment(seg);

    seg = new Segment(135, 175);
    seg->SetBearingIncr(2.5);
    mSpaceCruiser->AddSegment(seg);

    seg = new Segment(175, 185);
    seg->SetRollIncr(5.0);
    seg->SetBearingIncr(1.0);
    mSpaceCruiser->AddSegment(seg);

    seg = new Segment(185, 200);
    mSpaceCruiser->AddSegment(seg);

    seg = new Segment(200, 220);
    seg->SetRollIncr(5.0);
    seg->SetBearingIncr(3.0);
    mSpaceCruiser->AddSegment(seg);

    seg = new Segment(220, 240);
    seg->SetRollIncr(-5.0);
    seg->SetBearingIncr(3.0);
    mSpaceCruiser->AddSegment(seg);

    seg = new Segment(240);
    mSpaceCruiser->AddSegment(seg);

    // The Star cruiser can be added to the list of objects.
    mObjectsVector.Push(mSpaceCruiser);


/*
  ___ ___  _   ___ ___    ___   _ _____ ___
 / __| _ \/_\ / __| __|  / __| /_\_   _| __|
 \__ \  _/ _ \ (__| _|  | (_ |/ _ \| | | _|
 |___/_|/_/ \_\___|___|  \___/_/ \_\_| |___|

*/

    mGates = new SpaceGate(mScene, mResourcesCache, "Models/Gate.mdl");
    // Gate 1
    mGates->AddGate(Vector3(0,0,400));
    // Gate 2
    mGates->AddGate(Vector3(300, 0, 800), Quaternion(0, 45, 0));
    // Gate 3
    mGates->AddGate(Vector3(900, 0, 800), Quaternion(0, 90, 0));
    // Gate 4
    mGates->AddGate(Vector3(1700, 0, 900), Quaternion(0, 45, 0));
    // Gate 5
    mGates->AddGate(Vector3(1800, 0, 1600), Quaternion(0, 0, 0));
    // Gate 6
    mGates->AddGate(Vector3(1300, 0, 2200), Quaternion(0, -90, 0));
    // Gate 7
    mGates->AddGate(Vector3(700, 0, 1600), Quaternion(0, -90, 0));
    // Gate 8
    mGates->AddGate(Vector3(400, 0, 1700), Quaternion(0, -35, 0));
    // Gate 9
    mGates->AddGate(Vector3(300, 0, 2200), Quaternion(0, -40, 0));
    // Gate 10
    mGates->AddGate(Vector3(0, 0, 2400), Quaternion(0, -90, 0));
    // Gate 11
    mGates->AddGate(Vector3(-200, 0, 2200), Quaternion(0, -160, 0));
    // Gate 12
    mGates->AddGate(Vector3(-500, 0, 1800), Quaternion(0, -120, 0));
    // Gate 12'
    mGates->AddGate(Vector3(-800, 0, 1850), Quaternion(0, -60, 0));
    // Gate 13
    mGates->AddGate(Vector3(-1000, 0, 2100), Quaternion(0, -30, 0));
    // Gate 14
    mGates->AddGate(Vector3(-1200, 0, 2300), Quaternion(0, -90, 0));
    // Gate 15
    mGates->AddGate(Vector3(-1400, 0, 2000), Quaternion(0, -180, 0));
    // Gate 16
    mGates->AddGate(Vector3(-1500, 0, 1000), Quaternion(0, 170, 0));
    // Gate 17
    mGates->AddGate(Vector3(-1200, 0, 500), Quaternion(0, 140, 0));
    // Gate 18
    mGates->AddGate(Vector3(-900, 0, 100), Quaternion(0, 180, 0));

    // LOGI("Number of gates : " + String(mGates->GetNumInstances()));


/*
    _   ___ _____ _  _ ___ ___  ___  _  _   ___ _  _ ___ ___
   /_\ | __|_   _| || | __| _ \/ _ \| \| | / __| || |_ _| _ \
  / _ \| _|  | | | __ | _||   / (_) | .` | \__ \ __ || ||  _/
 /_/ \_\___| |_| |_||_|___|_|_\\___/|_|\_| |___/_||_|___|_|

*/

    SpaceObject *aetheron = new SpaceObject(mScene, mResourcesCache, "Aetheron", "Models/Aetheron.mdl");
    aetheron->SetPosition(1000.0, 80.0, 2740.0);
    aetheron->SetSpeed(5.0f);
    aetheron->SetDirection(0, -90);
    mObjectsVector.Push(aetheron);

    // Creation of the green semi-transparent arrow.
    mArrowNode = mScene->CreateChild("Arrow");
    mArrowNode->SetScale(5.0);
    StaticModel *arrow_model = mArrowNode->CreateComponent<StaticModel>();
    arrow_model->SetModel(mResourcesCache->GetResource<Model>("Models/GreenArrow.mdl"));
    arrow_model->ApplyMaterialList();
}

void SpaceGateApp::CreateEffects()
{
    // No effects for the moment, but such function is the right place for creating particle system for spaceship exhaust.
}

void SpaceGateApp::CreateSounds()
{

    // Create sound stuffs
    mSoundSource = mScene->CreateComponent<SoundSource>();
    mSoundSource->SetSoundType(SOUND_AMBIENT);

    mSoundBeep1 = mResourcesCache->GetResource<Sound>("Sounds/beep1.wav");
    mSoundSuccess = mResourcesCache->GetResource<Sound>("Sounds/success_sound.wav");
    mSoundWrongGate = mResourcesCache->GetResource<Sound>("Sounds/wrong_gate.wav");
    mSoundWrongSide = mResourcesCache->GetResource<Sound>("Sounds/this_is_not_the_right_side.wav");

    mRadioSource = mScene->CreateComponent<AudioSource>();
    mSoundGoodMorning = mResourcesCache->GetResource<Sound>("Sounds/en/michelle_good_morning_pilot.wav");
    mSoundOnboardComputer = mResourcesCache->GetResource<Sound>("Sounds/en/michelle_your_onboard_computer.wav");
    mSoundTargetLocked = mResourcesCache->GetResource<Sound>("Sounds/en/michelle_target_locked.wav");

    mSoundLowEngineRate = mResourcesCache->GetResource<Sound>("Sounds/engine_low_rate.wav");
    mSoundLowEngineRate->SetLooped(true);

    mSoundCollision1 = mResourcesCache->GetResource<Sound>("Sounds/collision_1.wav");

}

void SpaceGateApp::CreateHudSymbols()
{
    Font *font;
    Node *node;
    Texture2D *tex;
    Sprite *sprite;
    StaticSprite2D *sprite2D;

    // Create a node that will be at center position of the HUD.
    mHudCenterPosition = mModelNode->CreateChild("HudCenter");
    mHudCenterPosition->SetPosition(Vector3(0.0f, 0.4758439f, 3.06866f));

    // Create the node that will be used to display the tracked target reticle.
    mHudReticleTarget = mModelNode->CreateChild("HudTarget");
    mHudReticleTarget->SetPosition(HUD_CENTER_POS);
    mHudReticleTarget->SetScale2D(Vector2(0.005,0.005));
    sprite2D = mHudReticleTarget->CreateComponent<StaticSprite2D>();
    sprite2D->SetSprite(mResourcesCache->GetResource<Sprite2D>("Textures/hud_target.png"));

    node = mModelNode->CreateChild("CrossHair");
    node->SetPosition(HUD_CENTER_POS);
    node->SetScale2D(Vector2(0.01,0.01));
    sprite2D = node->CreateComponent<StaticSprite2D>();
    sprite2D->SetSprite(mResourcesCache->GetResource<Sprite2D>("Textures/cross.png"));

    node = mModelNode->CreateChild("CrossWheel");
    node->SetPosition(HUD_CENTER_POS);
    node->SetScale2D(Vector2(0.025, 0.025));
    sprite2D = node->CreateComponent<StaticSprite2D>();
    sprite2D->SetSprite(mResourcesCache->GetResource<Sprite2D>("Textures/crosswheel.png"));

    mHudTargetDirIndicator = mModelNode->CreateChild("TargetIndicator");
    mHudTargetDirIndicator->SetPosition(HUD_CENTER_POS);
    mHudTargetDirIndicator->SetScale2D(Vector2(0.0025,0.0025));
    sprite2D = mHudTargetDirIndicator->CreateComponent<StaticSprite2D>();
    sprite2D->SetSprite(mResourcesCache->GetResource<Sprite2D>("Textures/target_dir_indicator.png"));

    mSpeedIndicator = mModelNode->CreateChild("SpeedIndicator");
    mSpeedIndicator->SetPosition(HUD_CENTER_POS + Vector3(-0.025, 0.0, 0.0));
    mSpeedIndicator->SetScale2D(Vector2(0.0025, 0.0025));
    sprite2D = mSpeedIndicator->CreateComponent<StaticSprite2D>();
    sprite2D->SetSprite(mResourcesCache->GetResource<Sprite2D>("Textures/speed_indicator.png"));

    font = mResourcesCache->GetResource<Font>("Fonts/PCBius.ttf");

    node = mModelNode->CreateChild("SpeedText");
    node->SetPosition(HUD_CENTER_POS + Vector3(-0.1, 0.01, 0.0));
    node->SetScale(0.085);
    mHudSpeedText = node->CreateComponent<Text3D>();
    mHudSpeedText->SetColor(HUD_TEXT_COLOR);
    mHudSpeedText->SetText("XXXX");
    mHudSpeedText->SetFontSize(1.0f);
    mHudSpeedText->SetFont(font);

    node = mModelNode->CreateChild("RollText");
    node->SetPosition(HUD_CENTER_POS + Vector3(-0.1, 0.0, 0.0));
    node->SetScale(0.085);
    mHudRollText = node->CreateComponent<Text3D>();
    mHudRollText->SetColor(HUD_TEXT_COLOR);
    mHudRollText->SetText("XXXX");
    mHudRollText->SetFontSize(1.0f);
    mHudRollText->SetFont(font);

    node = mModelNode->CreateChild("TimerText");
    node->SetPosition(HUD_CENTER_POS + Vector3(-0.1, -0.05, 0.0));
    node->SetScale(0.085);
    mHudTimerText = node->CreateComponent<Text3D>();
    mHudTimerText->SetColor(HUD_TEXT_COLOR);
    mHudTimerText->SetText("XXXX");
    mHudTimerText->SetFontSize(1.0f);
    mHudTimerText->SetFont(font);

    node = mModelNode->CreateChild("TargetDistText");
    node->SetPosition(HUD_CENTER_POS + Vector3(0.1, 0.01, 0.0));
    node->SetScale(0.085);
    mHudDistText = node->CreateComponent<Text3D>();
    mHudDistText->SetColor(HUD_TEXT_COLOR);
    mHudDistText->SetText("XXXX");
    mHudDistText->SetFontSize(1.0f);
    mHudDistText->SetFont(font);

    node = mModelNode->CreateChild("TargetID");
    node->SetPosition(HUD_CENTER_POS + Vector3(-0.1, 0.03, 0.0));
    node->SetScale(0.085);
    mHudTargetID = node->CreateComponent<Text3D>();
    mHudTargetID->SetColor(HUD_TEXT_COLOR);
    mHudTargetID->SetText("XXXX");
    mHudTargetID->SetFontSize(1.0);
    mHudTargetID->SetFont(font);

    node = mModelNode->CreateChild("RadarRange");
    node->SetPosition(HUD_CENTER_POS + Vector3(0.075, 0.03, 0.0));
    node->SetScale(0.085);
    mHudRadarRange = node->CreateComponent<Text3D>();
    mHudRadarRange->SetColor(HUD_TEXT_COLOR);
    mHudRadarRange->SetText(String(mRadarRangesList[0]));
    mHudRadarRange->SetFontSize(1.0);
    mHudRadarRange->SetFont(font);
}

void SpaceGateApp::CreateInstructions()
{
    mChronoText = new Text(context_);
    mChronoText->SetText("Mission time: XXXX");
    mChronoText->SetFont(mResourcesCache->GetResource<Font>("Fonts/neuropol.ttf"));
    mChronoText->SetFontSize(20.0);
    mChronoText->SetColor(Color(1.0, 0.1, 0.1));
    mChronoText->SetPosition(IntVector2(20,20));
    mUIRoot->AddChild(mChronoText);

    // Create the instruction text that appears at the beginning.
    mInstructionText = new Text(context_);
    mInstructionText->SetText("Go through first gate to start mission time counter.\nThen fly as fast as possible through all gates !");
    mInstructionText->SetFont(mResourcesCache->GetResource<Font>("Fonts/tablaksh.ttf"));
    mInstructionText->SetFontSize(30.0);
    mInstructionText->SetColor(Color(1.0, 0.1, 0.1));
    mInstructionText->SetTextEffect(TE_STROKE);
    mInstructionText->SetEffectStrokeThickness(2);
    mInstructionText->SetEffectColor(Color(1.0,1.0,1.0));
    mInstructionText->SetAlignment(HA_CENTER, VA_CENTER);
    mUIRoot->AddChild(mInstructionText);

    mGameHelpText = new Text(context_);
    mGameHelpText->SetWordwrap(true);
    mGameHelpText->SetFixedWidth((int)(0.9 * mWindowSize.x_));
    mGameHelpText->SetFixedHeight((int)(0.9 * mWindowSize.y_));
    mGameHelpText->SetAlignment(HA_CENTER, VA_CENTER);
    mGameHelpText->SetFont(mResourcesCache->GetResource<Font>("Fonts/BlueHighway.ttf"));
    mGameHelpText->SetFontSize(20.0);
    mGameHelpText->SetTextEffect(TE_STROKE);
    mGameHelpText->SetEffectStrokeThickness(1);
    mGameHelpText->SetEffectColor(Color(0,0,0));
    mGameHelpText->SetText(String(help_text));
    mGameHelpText->SetVisible(false);
    mUIRoot->AddChild(mGameHelpText);
}

void SpaceGateApp::DrawWorldAxis()
{
    // Draw X axis
    float l = 5000.0f;
    float offset = 20.0f;
    Vector3 org = Vector3(0.0f, 0.0f, 0.0f);
    Vector3 endX = Vector3(l, 0.0f, 0.0f);
    Vector3 endY = Vector3(0.0f, l, 0.0f);
    Vector3 endZ = Vector3(0.0f, 0.0f, l);

    Color redcolor = Color(1,0,0);
    Color greencolor = Color(0,1,0);
    Color bluecolor = Color(0,0,1);

    mDebugRenderer->AddLine(org, endX, redcolor, true);
    mDebugRenderer->AddTriangle(endX, Vector3(l - offset, offset / 2, 0), Vector3(l - offset, -(offset / 2), 0), redcolor, true);
    mDebugRenderer->AddTriangle(endX, Vector3(l - offset, 0, offset / 2), Vector3(l - offset, 0, -(offset / 2)), redcolor, true);
    mDebugRenderer->AddLine(org, endY, greencolor, true);
    mDebugRenderer->AddTriangle(endY, Vector3(0, l - offset, offset / 2), Vector3(0, l - offset, -(offset / 2)), greencolor, true);
    mDebugRenderer->AddTriangle(endY, Vector3(offset / 2, l - offset, 0), Vector3(-(offset / 2), l - offset, 0), greencolor, true);
    mDebugRenderer->AddLine(org, endZ, bluecolor, true);
    mDebugRenderer->AddTriangle(endZ, Vector3(0, offset / 2, l - offset), Vector3(0, -(offset / 2), l - offset), bluecolor, true);
    mDebugRenderer->AddTriangle(endZ, Vector3(offset / 2, 0, l - offset), Vector3(-(offset / 2), 0, l - offset), bluecolor, true);

    mDebugRenderer->AddCross(endX - endZ, offset, redcolor, true);
    mDebugRenderer->AddCross(-endX, offset, redcolor, true);
    mDebugRenderer->AddCross(-endZ, offset, bluecolor, true);
    mDebugRenderer->AddCross(-endY, offset, greencolor, true);
}
