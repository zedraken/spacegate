#include "Segment.h"

void Segment::Init()
{
    mSpeedIncr = 0.0f;
    mElevationIncr = 0.0f;
    mBearingIncr = 0.0f;
    // mRollIncr = 0.0f;
    mLocalYawIncr = 0.0f;
    mLocalPitchIncr = 0.0f;
    mLocalRollIncr = 0.0f;
}

Segment::Segment(float aTmin, float aTmax)
{
    mTmin = aTmin;
    mTmax = aTmax;
    Init();
}

Segment::Segment(float aTmin)
{
    mTmin = aTmin;
    mTmax = -1.0f;
    Init();
}

Segment::~Segment()
{
    //dtor
}

void Segment::SetSpeedIncr(float aSpeedIncr)
{
    mSpeedIncr = aSpeedIncr;
}

void Segment::SetOrientationIncr(float aElevationIncr, float aBearingIncr)
{
    mElevationIncr = aElevationIncr;
    mBearingIncr = aBearingIncr;
}

void Segment::SetBearingIncr(float aBearingIncr)
{
    mBearingIncr = aBearingIncr;
}

void Segment::SetElevationIncr(float aElevationIncr)
{
    mElevationIncr = aElevationIncr;
}

void Segment::ResetOrientation()
{
    mElevationIncr = 0;
    mBearingIncr = 0;
}

void Segment::SetRotationIncr(float aYawIncr, float aPitchIncr, float aRollIncr)
{
    mLocalYawIncr = aYawIncr;
    mLocalPitchIncr = aPitchIncr;
    mLocalRollIncr = aRollIncr;
}

void Segment::SetYawIncr(float aYawIncr)
{
    mLocalYawIncr = aYawIncr;
}

void Segment::SetPitchIncr(float aPitchIncr)
{
    mLocalPitchIncr = aPitchIncr;
}

void Segment::SetRollIncr(float aRollIncr)
{
    mLocalRollIncr = aRollIncr;
}

void Segment::ResetRotationIncr()
{
    mLocalYawIncr = 0.0f;
    mLocalPitchIncr = 0.0f;
    mLocalRollIncr = 0.0f;
}

bool Segment::IsInRange(float aTime)
{
    if(mTmax < 0)
    {
        if(aTime >= mTmin)
            return(true);
        else
            return(false);
    }
    else if(aTime >= mTmin && aTime < mTmax)
        return(true);
    else
        return(false);
}

float Segment::GetSpeedIncr()
{
    return mSpeedIncr;
}

float Segment::GetElevationIncr()
{
    return mElevationIncr;
}

float Segment::GetBearingIncr()
{
    return mBearingIncr;
}

float Segment::GetLocalYawIncr()
{
    return mLocalYawIncr;
}

float Segment::GetLocalPitchIncr()
{
    return mLocalPitchIncr;
}

float Segment::GetLocalRollIncr()
{
    return mLocalRollIncr;
}
