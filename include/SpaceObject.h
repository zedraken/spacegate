#ifndef SpaceObject_h
#define SpaceObject_h

#include <Urho3D/Scene/Scene.h>
#include <Urho3D/Container/Str.h>
#include <Urho3D/Graphics/StaticModel.h>
#include <Urho3D/Physics/RigidBody.h>
#include <Urho3D/Physics/CollisionShape.h>
#include <Urho3D/Resource/ResourceCache.h>
#include <Urho3D/Graphics/Model.h>
#include <Urho3D/Math/Quaternion.h>
#include <Urho3D/IO/Log.h>

#include "Segment.h"


#define LOGI(__text__) URHO3D_LOGINFO(__text__)
#define LOGD(__text__) URHO3D_LOGDEBUG(__text__)


using namespace Urho3D;

/*!
The SpaceObject class
\brief That class is used to handle one space object which can be an asteroid or a space ship.
Here is an example of how to create an object instance and how to use some functions.
\code
SpaceObject *object = new(parent, cache, "Object", "models/object.mdl");
object->SetPosition(100, -40, 0);
object->SetRotation(15, 0, -5);
object->SetSpeed(250);
object->SetDirection(45, 0);
\endcode
An object has its own 'Update' function that shall be called within the main 'Update' function.
*/
class SpaceObject {

 public:

    /*!
    \fn SpaceObject()
    \brief Constructor of the SpaceObject class
    \param aScene a pointer to the parent used to create the nodes (usually, it is the scene)
    \param aResCache a pointer to the resource cache (to access the various resources)
    \param aModelName a string representing the object name
    \param aModelFile a Sstring containing model filename (.mdl file), with path
    */
    SpaceObject(Scene *aScene, ResourceCache *aResCache, String aModelName, String aModelFile);

    /*!
    \fn void SetPosition()
    \brief Set the initial object position in absolute space
    \param aPosX the position along the X axis
    \param aPosY the position along the Y axis
    \param aPosZ the position along the Z axis
    */
    virtual void SetPosition(float aPosX, float aPosY, float aPosZ);

    /*!
    \fn void SetSpeed()
    \brief Sets the initial object speed
    \param aSpeed the speed in meters per seconds
    */
    virtual void SetSpeed(float aSpeed);

    /*!
    \fn void SetDirection()
    \brief Set direction of object's trajectory
    \param aElevation the elevation angle above horizontal plane
    \param aBearing the heading
    */
    virtual void SetDirection(float aElevation, float aBearing);

    /*!
    \fn void SetElevation()
    \brief Set the object's elevation angle of its direction vector.
    \param aElevation the elevation angle above the horizontal plane
    */
    virtual void SetElevation(float aElevation);

    /*!
    \fn void SetBearing()
    \brief Set the object's bearing angle
    \param aBearing the bearing angle
    */
    virtual void SetBearing(float aBearing);

    /*!
    \fn void SetRotation()
    \brief Set the self rotation angles of the objects
    \param aYaw rotation angle around X axis
    \param aPitch rotation angle around Y axis
    \param aRoll rotation angle arount Z axis
    \note The forward direction is the Z positive axis
    */
    virtual void SetRotation(float aYaw, float aPitch, float aRoll);

    /*!
    \fn void SetPitchAngle()
    \brief Set the object's pitch angle (self rotation around its local X axis).
    \param aAngle the pitch angle in degrees.
    */
    virtual void SetPitchAngle(float aAngle);

    /*!
    \fn void SetYawAngle()
    \brief Set the object's yaw angle (self rotation around its local Y axis).
    \param aAngle the yaw angle in degrees.
    */
    virtual void SetYawAngle(float aAngle);

    /*!
    \fn void SetRollAngle()
    \brief Set the object's roll angle (self rotation around its local Z axis).
    \param aAngle the roll angle in degrees.
    */
    virtual void SetRollAngle(float aAngle);

    /*!
    \fn Vector3 GetPosition()
    \brief Return the object's position in the absolute space.
    \return Returns a Vector3 type containing the absolute position.
    */
    virtual Vector3 GetPosition();

    /*!
    \fn unsigned int GetDistanceToOrigin()
    \brief Return the distance of the object to the origin of space.
    \return Returns distance to origin.
    */
    virtual float GetDistanceToOrigin();

    /*!
    \fn float GetSpeed()
    \brief Return the object's speed.
    \return Return the object's speed in meters per second.
    */
    virtual float GetSpeed();

    /*!
    \fn float GetElevation()
    \brief Return the object's elevation angle in degrees above the horizontal plane.
    \return Return the object's elevation angle.
    */
    virtual float GetElevation();

    /*!
    \fn float GetBearing()
    \brief Return the object's bearing angle in degrees (heading).
    \return Return the object's bearing angle.
    */
    virtual float GetBearing();

    /*!
    \fn void Update()
    \brief Update the object's trajectory and rotation by checking its trajectory vectors.

    That function shall be called each time the rendering is updated in order to compute the
    object new position and orientation based on its speed, direction and elevation.

    \param aTimeStep the elapsed time step in milliseconds.

    The code snippet below is a typical example on how to use the function in the frame
    of the PostUpdate() function called by Urho3D engine.

    \code
    void myTestApp::PostUpdate(StringHash eventType, VariantMap& eventData)
    {
        // Get elapsed time from last update
        float time_step = eventData[P_TIMESTEP].GetFloat();
        // Update object parameters using elapsed time
        object->Update(time_step);
    }
    \endcode
    */
    virtual void Update(float aTimeStep);

    /*!
    \fn void Enable()
    \brief Enable the object (it will be rendered).
    */
    virtual void Enable();

    /*!
    \fn void Disable()
    \brief Disable the object (it will not be rendered).
    */
    virtual void Disable();

    /*!
    \fn bool IsEnabled()
    \brief Gets the status of the object (is it enabled or disabled)
    \return Returns 'true' if the object is enabled, or 'false' otherwise.
    */
    virtual bool IsEnabled();

    /*!
    \fn void AddSegment()
    \brief Add a new segment into the object's trajectory.
    \param aSegment a pointer on the segment to add.
    */
    virtual void AddSegment(Segment *aSegment);

    /*!
    \fn int GetNumSegments()
    \brief Get the number of segments in the object's trajectory.
    \return Returns the number of segments.
    */
    virtual int GetNumSegments();

    /*!
    \fn void CreatePhysicalBody()
    \brief Create a rigid body and a collision shape to the object.

    By default, a new object does neither have any rigid body nor a collision shape.
    With that method, it is possible to request a physical body creation and a collision
    shape that will be used for collision detection. Note that the collision shape is the
    model itself.
    \param mass the object mass in kilograms.
    \param shape the shape to use for collision detection (see Urho3D/include/Physics/CollisionShape.h)
    */
    void CreatePhysicalBody(float mass, ShapeType shape);

    /*!
    \fn void CreatePhysicalBody()
    \brief Create a rigid body with a bounding box as the collision shape (default)
    \param mass the object mass used by the physic part
    */
    void CreatePhysicalBody(float mass);

    /*!
    \fn Node* GetNode()
    \brief Returns the node associated with that object.
    \return Returns an Node pointer.
    */
    inline Node* GetNode() {
        return(mModelNode);
    }

    /*!
    \fn GetCollisionShape()
    \brief Returns a pointer to the collision shape
    \return The collision shape pointer
    */
    inline CollisionShape *GetCollisionShape() {
        return (CollisionShape*)mModelNode->GetComponent<CollisionShape>();
    }

 private:
    SharedPtr<StaticModel> mStaticModel;        /*!< The object's static model */
    String mModelFile;                          /*!< The object's .mdl file */
    SharedPtr<Node> mModelNode;                 /*!< The model's node */
    String mModelName;      /*!< The model's name */

    float mSpeed;           /*!< Model's speed */

    float mElevation;       /*!< Model's elevation angle (around parent's X axis) used for forward vector */
    float mBearing;         /*!< Model's bearing angle (around parent's Y axis) used for forward vector */

    float mLocalYaw;        /*!< Model's yaw angle (around local X axis) */
    float mLocalPitch;      /*!< Model's pitch angle (around local X axis) */
    float mLocalRoll;       /*!< Model's roll angle (around local Z axis) */

    float mAbsTime;         /*!< Absolute simulation time */

    Vector<Segment*> *mTrajectory;  /*!< The vector of segments = a trajectory */

    DebugRenderer *mDebugRenderer;

};

#endif // SpaceObject_h
