#ifndef SPACEGATE_H
#define SPACEGATE_H

#include <Urho3D/Scene/Scene.h>
#include <Urho3D/Resource/ResourceCache.h>
#include <Urho3D/Container/Str.h>
#include <Urho3D/Container/Vector.h>
#include <Urho3D/Math/BoundingBox.h>
#include <Urho3D/Math/Quaternion.h>
#include <Urho3D/Math/Vector3.h>
#include <Urho3D/Graphics/Model.h>
#include <Urho3D/Graphics/StaticModelGroup.h>
#include <Urho3D/Physics/CollisionShape.h>
#include <Urho3D/Physics/RigidBody.h>
#include <Urho3D/Scene/Node.h>


using namespace Urho3D;

/*!
\brief The SpaceGate class. That class is used to handle a set of gates based on the same 3D model by creating
an instance of a StaticGroupNode for optimization.

Here is an example below on how to use that class:
\code
// Create a SpaceGate object. All gates created within that instance will use the same
// model "Models/Gate.mdl".
SpaceGate *gates = new SpaceGate(mScene, mResourcesCache, "Models/Gate.mdl");
// Add first gate at default position with default orientation
gates->AddGate();
// Add second gate at position (0, 0, 400) with a rotation of 90° around its Y axis
gates->AddGate(Vector3(0,0,400), Quaternion(0,90,0));
// Add third gate at position (10, -100, 200) with default orientation
gates->AddGate(Vector3(10, -100, 200));
\endcode
*/

class SpaceGate
{
    public:
        /*!
        \fn SpaceGate(Scene *parent, ResourceCache *resource, String modelfile);
        \brief Builds a Gate object.

        Builds a gate object which will contain all gates on the same provided model.
        \param parent pointer to the parent scene
        \param resource pointer to the resource cache subsystem
        \param modelfile model file name
        */
        SpaceGate(Scene *parent, ResourceCache *resource, String modelfile);
        /*!
        \fn ~SpaceGate();
        \brief Gate class destructor
        */
        virtual ~SpaceGate();
        /*!
        \fn AddGate();
        \brief Create a new gate with default position and orientation and add it into gates list.
        */
        virtual void AddGate();
        /*!
        \fn AddGate(Vector3 position, Quaternion rotation);
        \brief Create a new gate at specified position with specified orientation, and add it into gates list.
        \param position a Vector3 type containing the position of the gate
        \param rotation a Quaternion type containing the gate orientation around its local axis
        */
        virtual void AddGate(Vector3 position, Quaternion rotation);
        /*!
        \fn AddGate(Vector3 position);
        \brief Create a new gate at specified position with default orientation, and add it into gates list.
        \param position a Vector3 type containing the position of the gate
        */
        virtual void AddGate(Vector3 position);
        /*!
        \fn GetNumInstances();
        \brief Returns the number of gates
        \return Returns an unsigned integer value containing the number of gates
        */
        virtual unsigned int GetNumInstances();

        /*!
        \fn GetNode(unsigned index);
        \brief Get the node pointer of the specified gate.
        \param index the gate index (starting from 0)
        \return Returns the node pointer.
        */
        virtual Node *GetNode(unsigned index);

    protected:

    private:
        Node *gateCreateAndAdd();

        SharedPtr<Scene> mParent;
        SharedPtr<ResourceCache> mResource;
        SharedPtr<Node> mGroupNode;
        SharedPtr<StaticModelGroup> mGroupModel;

        int mNum;

        Vector<SharedPtr<Node> > gateNodes_;

};

#endif // SPACEGATE_H
