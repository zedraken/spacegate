
#pragma once

#define LOGI(__text__) URHO3D_LOGINFO(__text__)
#define LOGD(__text__) URHO3D_LOGDEBUG(__text__)
#define LOGE(__text__) URHO3D_LOGERROR(__text__)

