#ifndef AUDIOSOURCE_H
#define AUDIOSOURCE_H

#include <Urho3D/Urho3DAll.h>


class AudioSource : public SoundSource
{
    URHO3D_OBJECT(AudioSource, SoundSource);

    public:
        AudioSource(Context *context);
        virtual ~AudioSource();
        virtual void Playback(SharedPtr<Sound> sample);
        void PlaybackEndEvent(StringHash eventType, VariantMap& eventData);
        void PostUpdateEvent(StringHash eventType, VariantMap& eventData);
        float GetElapsedTime();

    protected:

    private:
        List<SharedPtr<Sound> > mSampleList;
        Timer mTimer;
        float mElapsedTime;
        float mStartTime;
        bool mDelayPlayback;
};

#endif // AUDIOSOURCE_H
