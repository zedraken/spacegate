#ifndef TEST01APP_H
#define TEST01APP_H


#pragma once
#include "SpaceObject.h"
#include "SpaceGate.h"
#include "AudioSource.h"
#include "macros.h"

using namespace Urho3D;



const Urho3D::Vector3 CAMPOS {0.0f, 0.4788176f, 2.66945f};
const Urho3D::Vector3 CAMPOSFRONT {0.0f, 0.24821f, 3.6947f};
const Urho3D::Vector3 CAMPOSREAR {0.0f, 0.51897f, -3.1775f};
/*
const Vector<Vector3>   DisplayCameraList = {{0.0f, 0.4788176f, 2.66945f}};
const Vector<float>     DisplayCameraRotateList = {0, 180};
*/
const Vector3   HUD_CENTER_POS(0.0f, 0.4758439f, 3.06866f);
const Color     HUD_TEXT_COLOR(0.0f, 1.0f, 0.0f);

/*!
\class SpaceGateApp
\brief The main class for the Space Gate game.
*/
class SpaceGateApp : public Application
{
    URHO3D_OBJECT(SpaceGateApp, Application);

    public:

        SpaceGateApp(Context *context);
        virtual void Setup();
        virtual void Start();
        virtual void Stop();
        void ProcessCLIArguments();
        void HandleKeyDown(StringHash eventType, VariantMap& eventData);
        void MouseWheelEvent(StringHash eventType, VariantMap& eventData);
        void MouseButtonDown(StringHash eventType, VariantMap& eventData);
        void Update(StringHash eventType, VariantMap& eventData);
        void RenderUpdate(StringHash eventType, VariantMap& eventData);
        void PostRenderUpdate(StringHash eventType, VariantMap& eventData);
        void JoystickButtonDown(StringHash eventType, VariantMap& eventData);
        void JoystickButtonUp(StringHash eventType, VariantMap& eventData);
        void JoystickAxisMove(StringHash eventType, VariantMap& eventData);
        void NodeCollisionStartEvent(StringHash eventType, VariantMap& eventData);
        void QuitMessageAck(StringHash eventType, VariantMap& eventData);
        void PrintText(const char *str);
        void PrintText(String str);
        void CreateScene();
        void CreateInstructions();
        void CreateObjects();
        void CreateEffects();
        void CreateSounds();
        void CreateHudSymbols();
        void UpdateObjects(float time_step);
        void ToggleMouseVisible();

        /*!
        \fn SplashScreen()
        \brief Display the splash screen and waits few seconds
        */
        void SplashScreen();

        void HandleSplashScreen(StringHash eventType, VariantMap& eventData);

        /*!
        \fn UpdateTrajectory(float time_step);
        \brief Update the ship trajectory and orientation.
        \param time_step the elapsed time in ms from last call
        */
        void UpdateTrajectory(float time_step);
        /*!
        \fn DrawWorldAxis();
        \brief Draw the world axis X, Y and Z.
        */
        void DrawWorldAxis();
        /*!
        \fn ComputeAngularRotation(float angle)
        \brief Compute the angular increment based on the input device (joystick, mouse) deviation.
        \param deviation the deviation of the joystick/mouse from the central point
        \param rotation_speed the angle rotation rate in degrees per seconds
        \param time_step the time step simulation time (elapsed time from last computation)
        \return Returns the angular increment to be added to the appropriate angle.
        */
        float ComputeAngularRotation(float deviation, float rotation_speed, float time_step);

        /*!
        \fn ComputeAngularPitch(float deviation, float rotation_speed, float time_step)
        \brief Compute angular pitch rotation (around X axis)
        \param deviation the deviation of the joystick/mouse from the central point
        \param rotation_speed the angle rotation rate in degrees per seconds
        \param time_step the time step simulation time (elapsed time from last computation)
        \return Returns the angular increment to be added to the appropriate angle.
        */
        float ComputeAngularPitch(float deviation, float rotation_speed, float time_step);

        /*!
        \fn ComputeHudTargetIndicator()
        \brief Compute target indicator on the HUD.
        That function calculate the position of the squared reticle that is displayed on the HUD.
        Such reticle is used to show the relative position of the tracked object to the pilot,
        using the actual ship position and orientation.
        \return Returns the absolute position of the reticle in world reference.
        */
        void UpdateHudDisplay();

        /*!
        \fn SelectNextTarget()
        \brief Select next target in the list of targets.
        */
        void SelectNextTarget();

        /*!
        \fn SelectPreviousTarget()
        \brief Select previous target in the list of targets.
        */
        void SelectPreviousTarget();

        /*!
        \fn DisplayInstructions()
        \brief Displays game instructions.
        */
        void DisplayInstructions();

        /*!
        \fn SelectNextRadarRange();
        \brief Select next radar range in the list of available ranges.
        Loop to the first range if last one is reached.
        */
        void SelectNextRadarRange();

        /*!
        \fn SelectPreviousRadarRange();
        \brief Select previous radar range in the list of available ranges.
        Loop to the last one if the first one is reached.
        */
        void SelectPreviousRadarRange();

    protected:
    private:
        Input *mInput;                      /*!< Pointer to the static input subsystem. */
        SharedPtr<Scene> mScene;            /*!< The global scene. */
        SharedPtr<Node> mCameraNode;        /*!< The camera node. */
        SharedPtr<Camera> mCamera;          /*!< The camera object. */
        SharedPtr<Node> mFrontCameraNode;   /*!< The optical front camera node. */
        SharedPtr<Camera> mFrontCamera;     /*!< The optical front camera. */
        SharedPtr<Node> mRearCameraNode;    /*!< The rear camera node. */
        SharedPtr<Camera> mRearCamera;      /*!< The rear camera looking at the spaceship rear */
        SharedPtr<Node> mModelNode;         /*!< The main model node (the ship). */
        SharedPtr<StaticModel> modelA;      /*!< The main static model shape (the ship). */
        SharedPtr<RigidBody> mShipBody;     /*!< The rigid body for physic to be applied to the ship. */
        SharedPtr<CollisionShape> mCollisionShape;  /*!< The ship shape used for collision detection. */
        SharedPtr<Node> mInstrPanelNode;    /*!< The instrument panel node. */
        SharedPtr<StaticModel> mInstrPanelModel;    /*!< The model shape of the instrument panel. */
        SharedPtr<ResourceCache> mResourcesCache;   /*!< Pointer to the resource cache. */
        SharedPtr<UIElement> mUIRoot;       /*!< Pointer to the User Interface root. */
        SharedPtr<Zone> mZone;              /*!< The zone area. */
        SharedPtr<Window> mWinMsg;
        SharedPtr<Node> mArrowNode;         /*!< The big green arrow that indicates next gate to cross. */
        SharedPtr<SoundListener> mTheListener;  /*!< The listener, for 3D sound sources. */
        DebugRenderer *mDebugRenderer;
        Timer *mTimer;              /*!< Timer to measure simulation time. */
        Timer *mDisplayTimer;       /*!< Timer to display UI items during a limited amount of time. */
        bool mDisplayState;         /*!< A boolean indicating that there is an temporary item displayed. */
        SpaceGate *mGates;        /*!< The list of gates to go through. */
        Vector3 mNextGateIndicator;  /*!< 3D position in space of the next gate to cross. */
        float mAnimationAngleIndicator; /*!< An angle value that is used to animate the arrow indicating the next gate to go through. */
        SharedPtr<DebugHud> mDebugHud;
        SharedPtr<Viewport> mViewPort;      /*!< The view port used for rendering. */
        SharedPtr<Viewport> mHeadDownDisplayViewPort;   /*!< The viewport used for head down display. */
        bool mHeadDownDisplayToggle;    /*!< Boolean used to switch between front and rear display for head down display. */
        bool debug_render;      /*!< Indicate if debug informations shall be displayed. */
        float mYaw;              /*!< Yaw angle (around Y axis) for camera. */
        float mPitch;            /*!< Pitch angle (around X axis) for camera. */
        float mCamSpeed;        /*!< Camera displacement speed. */
        float mShipSpeed;       /*!< Absolute ship speed. */
        float mShipSpeedIncr;   /*!< Speed increment. */
        float mShipSpeedRequest;/*!< Requested speed. */
        float mShipVx;          /*!< Ship speed along X axis. */
        float mShipVy;          /*!< Ship speed along Y axis. */
        float mShipVz;          /*!< Ship speed along Z axis. */
        float mShipYaw;         /*!< Ship rotation angle around Y axis. */
        float mShipPitch;       /*!< Ship rotation angle around Y axis. */
        float mShipRoll;        /*!< Ship rotation angle around Z axis. */
        float mShipYawIncr;     /*!< Ship's yaw angle increment (around X axis). */
        float mShipPitchIncr;   /*!< Ship's pitch angle increment (around Y axis). */
        float mShipRollIncr;    /*!< Ship's roll angle increment (around Z axis). */
        float mShipAngularThreshold;    /*!< Angular ship movement threshold. */
        bool mMouseVisible;     /*!< Toggle to indicate if mouse pointer shall be displayed. */
        float mDisplayZoom;       /*!< Zoom value for head down display. Can be modified with CTRL + mouse wheel. */
        unsigned int mNextGateToCross;  /*!< Contains the index of the next gate to go though. */
        bool mVirtualDisplay;	/*!< Indicate if a VR headset has to be used (default: false). */
        bool mNoJoystick;     /*!< Indicate if the joystick presence test shall be skipped. */
        Urho3D::Vector<SpaceObject*> mObjectsVector;    /*!< The list of space objects (does not include gates). */
        JoystickState *mJoystick;       /*!< Active joystick */
        IntVector2 mWindowSize;         /*!< Viewport window size */
        SpaceObject *mTrackedObject;        /*!< Tracked object on the radar (appears on the HUD). */
        unsigned int mTrackedObjectIndex;   /*!< Index of the currently tracked object on the radar. */
        SpaceObject *mSpaceCruiser;     /*!< The object for the Space Cruiser. */
        bool mMissionStarted;           /*!< Set to true if mission has started (by going through first gate). */
        unsigned mMissionStartTime;     /*!< Store mission start time. */
        unsigned mMissionCurrentTime;   /*!< Store mission current time in seconds (if it has started). */
        unsigned mRadarRangeIndex;           /*!< Radar range index */
        Vector<int> mRadarRangesList;  /*!< Radar range list */
        unsigned mParentIndex;          /*!< Index used to loop through available parents. */
        bool mShipBumped;               /*!< Indicate if the spaceship has been collided by another object. */
        // HUD objects
        SharedPtr<Node> mHudReticleTarget;
        SharedPtr<Node> mHudTargetDirIndicator;
        SharedPtr<Node> mHudCenterPosition;
        SharedPtr<Node> mPilotEyePosition;
        SharedPtr<Node> mSpeedIndicator;
        // Objects to handle text that appears on the HUD
        SharedPtr<Text3D> mHudSpeedText;
        SharedPtr<Text3D> mHudRollText;
        SharedPtr<Text3D> mHudDistText;
        SharedPtr<Text3D> mHudTimerText;
        SharedPtr<Text3D> mHudTargetID;
        SharedPtr<Text3D> mHudRadarRange;
        // Other UI elements
        SharedPtr<Text> mChronoText;
        SharedPtr<Text> mInstructionText;
        SharedPtr<Text> mGameHelpText;
        // Sound
        SharedPtr<SoundSource> mSoundSource;    /*!< The sound source for spaceship internal sounds. */
        SharedPtr<SoundSource3D> mSndSource3D1; /*!< One 3D sound source. */
        SharedPtr<AudioSource> mRadioSource;    /*!< The sound source for radio messages. */
        SharedPtr<Sound> mSoundBeep1;
        SharedPtr<Sound> mSoundSuccess;
        SharedPtr<Sound> mSoundFailure;
        SharedPtr<Sound> mSoundWrongGate;   /*!< Radio message "wrong gate". */
        SharedPtr<Sound> mSoundWrongSide;   /*!< Radio message "not the right side". */
        SharedPtr<Sound> mSoundGoodMorning;
        SharedPtr<Sound> mSoundOnboardComputer;
        SharedPtr<Sound> mSoundTargetLocked;    /*!< "Target locked" */
        SharedPtr<Sound> mSoundLowEngineRate;
        SharedPtr<Sound> mSoundCollision1;
        // Particle emitter objects
        SharedPtr<ParticleEmitter> mLeftExhaust;
        SharedPtr<ParticleEmitter> mRightExhaust;
};

#endif // TEST01APP_H
