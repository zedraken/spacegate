#ifndef SEGMENT_H
#define SEGMENT_H

/*!
The Segment class.
\brief That class is used to handle an object trajectory segment (meaning in a straight direction).
Adding several instances of that class builds a complete trajectory.
Moreover, for each segment, it is possible to specify the object own local orientation, trajectory angles
and speed modifications. The current class handles those values for one single segment.
If a trajectory is made of several segments, then several instances of that class shall be created and
gathered into an array or a vector (the example below uses a Vector class to handel all the segments).

\code
Vector<Segment*> trajectory;
Segment *seg;
// Segment n°1
seg = new Segment(0, 10);
trajectory.Push(seg);
// Segment n°2
seg = new Segment(10, 40);
seg->SetSpeedIncr(10.0f);
seg->SetElevationIncr(5.0);
trajectory.Push(seg);
\endcode
In the above example, we create two segments.
The first one represents the object trajectory for simulation time between 0 and 10 seconds.
Since we do not specify any angle or speed increments, the objects will neither move nor rotate.
The second segment that will control the object's trajectory
between 10 seconds and 40 seconds. During that period of time, the object will
accelerate with a rate of 10 meters per seconds, and modify its elevation angle by
5 degrees per second.
Note that we store each segment (the pointer in fact) into a Vector array for further use.
 */
class Segment
{
    public:
        /*! \fn Segment(float aTmin, float aTmax)
         * \brief Constructor for a new segment with low and up time range.
         * \param aTmin beginning time of the segment
         * \param aTmax end time of the segment
        */
        Segment(float aTmin, float aTmax);

        /*! \fn Segment(float aTmin)
         * \brief Constructor for a new segment with low limit only.
         * \param aTmin the begining time of the segment.
         */
        Segment(float aTmin);

        /*! \fn ~Segment()
         * \brief The class descructor
         */
        virtual ~Segment();

        /*! \fn SetSpeedIncr(float aSpeedIncr)
         * \brief Set Speed increment
         * \param aSpeedIncr speed increment for that segment
         */
        void SetSpeedIncr(float aSpeedIncr);

        /*! \fn SetOrientationIncr(float aElevationIncr, float aBearingIncr)
         * \brief Set orientation increment (orientation is used for object trajectory: elevation and bearing)
         * \param aElevationIncr the elevation angle increment in degrees per seconds
         * \param aBearingIncr the bearing angle increment in degrees per seconds
         */
        void SetOrientationIncr(float aElevationIncr, float aBearingIncr);

        /*! \fn SetElevationIncr(float aElevationIncr)
         * \brief Set elevation angle increment.
         * \param aElevationIncr the elevation angle increment in degrees per seconds
         */
        void SetElevationIncr(float aElevationIncr);

        /*! \fn SetBearingIncr(float aBearingIncr)
         * \brief Set bearing angle increment.
         * \param aBearingIncr the bearing angle increment in degrees per seconds
         */
        void SetBearingIncr(float aBearingIncr);

        /*! \fn ResetOrientation()
         * \brief Reset orientation angles increments (set to 0)
         */
        void ResetOrientation();

        /*! \fn SetRotationIncr(float aYawIncr, float aPitchIncr, float aRollIncr)
         * \brief Set rotation increment (object self rotation around its local axis)
         *
         * Sets the angle increments for rotations of the object around its local X, Y and Z axis.
         * At each rendering iteration, each local angle is incremented by the corresponding
         * increment value multiplied by the time step.
         * The angles here are independent from the objects orientation.
         *
         * \note The increment values can be negative.
         *
         * \param aYawIncr the rotation angle increment around X axis
         * \param aPitchIncr the rotation angle increment around Y axis
         * \param aRollIncr the rotation angle increment around Z axis
         */
        void SetRotationIncr(float aYawIncr, float aPitchIncr, float aRollIncr);

        /*! \fn SetYawIncr(float aYawIncr)
         * \brief Set rotation angle increment around X axis.
         * \param aYawIncr the yaw angle increment (rotation around X local axis)
         */
        void SetYawIncr(float aYawIncr);

        /*! \fn SetPitchIncr(float aPitchIncr)
         * \brief Set rotation angle increment around Y axis.
         * \param aPitchIncr the pitch angle increment (rotation around Y local axis)
         */
        void SetPitchIncr(float aPitchIncr);

        /*! \fn SetRollIncr(float aRollIncr)
         * \brief Set rotation angle increment around Z axis.
         * \param aRollIncr the roll angle increment (rotation around Z local axis)
         */
        void SetRollIncr(float aRollIncr);

        /*! \fn ResetRotationIncr()
         * \brief Reset rotation angle increments (set increments to 0).
         */
        void ResetRotationIncr();

        /*! \fn IsInRange(float aTime)
         * \brief Check if provided time is within current segment range.
         * \param aTime simulation time in seconds
         * \return Returns true if the provided simulation time is within current segment
         * otherwise returns false.
         */
        bool IsInRange(float aTime);

        /*! \fn GetSpeedIncr()
         * \brief Get the speed increment value.
         * \return Returns a float value representing the speed increment in meters per seconds.
         */
        float GetSpeedIncr();

        /*! \fn GetElevationIncr()
         * \brief Get the orientation yaw angle increment.
         * \return Returns a float value representing the elevation angle increment in degrees per seconds.
         */
        float GetElevationIncr();

        /*! \fn GetBearingIncr()
         * \brief Get the orientation pitch angle increment.
         * \return Returns a float value representing the bearing angle increment in degrees per seconds.
         */
        float GetBearingIncr();

        /*! \fn GetLocalYawIncr()
         * \brief Get the rotation yaw angle increment.
         * \return Returns a float value representing the local rotation angle increment around X axis (yaw).
         */
        float GetLocalYawIncr();

        /*! \fn GetLocalPitchIncr()
         * \brief Get the rotation pitch angle increment.
         * \return Returns a float value representing the local rotation angle increment around Y axis (pitch).
         */
        float GetLocalPitchIncr();

        /*! \fn GetLocalRollIncr()
         * \brief Get the rotation roll angle increment.
         * \return Returns a float value representing the local rotation angle increment around Z axis (roll).
         */
        float GetLocalRollIncr();

    protected:

    private:
        void Init();

        float mTmin;
        float mTmax;

        float mSpeedIncr;

        float mElevationIncr;
        float mBearingIncr;

        float mLocalYawIncr;
        float mLocalPitchIncr;
        float mLocalRollIncr;
};

#endif // SEGMENT_H
